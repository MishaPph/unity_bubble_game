# Neon Bubble Game
There is a bubble shooter game with a merge element.
It's easy to play and very addictive.

## How to play
- Shot the bubble to the bubble with the identification numbers and merge them.
- The number with the largest on the level will be automatically destroyed.
- Clear the board.

There are 2 special bubbles.
(A) bubble - can be any number, as in the neighbors
(B) bubble -  destroys all tangential bubbles

## About the project
This is my project where I am trying to create a game from scratch to release it as fast as possible.
All assets have been created by myself except SFX.
Also, I used a unit test to cover a part of the project.
Now it's available on [Google Play Store](https://play.google.com/store/apps/details?id=com.Foggy.NeonBubble)
