using Model;

namespace Tests
{
    public class PieceSpawnerFake : IPieceSpawner {
        
        private readonly Piece _value;
        public IPiece CreateBasicPiece() {
            return _value;
        }

        public PieceSpawnerFake(int value)
        {
            _value = new Piece(value);
            Piece.Base = 2;
            Piece.Max = Max;
        }
        
        public int Max => 13;
        public int MaxValue => Max;
        public IPiece CreatePiece(int type)
        {
            return new Piece(type);
        }
        public IPiece CreateTopPiece()
        {
            return CreateBasicPiece();
        }
        public IPiece CreateUserPiece(bool lastPhase, int minTypeOnBoard)
        {
            return CreateBasicPiece();
        }
    }
}