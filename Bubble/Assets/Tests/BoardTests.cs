using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using NUnit.Framework;
using UnityEngine;

namespace Tests {
    
    public class BoardTests {
        
        [Test]
        public void Width_GivenBoardDefinition_ShouldReturnLengthOfXAxis() {
            // Arrange
            int[,] state = {
                {0, 0, 0}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act
            var width = board.Width;
            
            // Assert
            Assert.That(width, Is.EqualTo(3));
        }
        
        [Test]
        public void Height_GivenBoardDefinition_ShouldReturnLengthOfYAxis() {
            // Arrange
            int[,] state = {
                {0, 0, 0},
                {0, 0, 0}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act
            var height = board.Height;
            
            // Assert
            Assert.That(height, Is.EqualTo(2));
        }

        [TestCase( 0, 0, ExpectedResult = 1, TestName = "Coordinate: (0,0) = 1" )]
        [TestCase( 1, 1, ExpectedResult = 2, TestName = "Coordinate: (1,1) = 2" )]
        [TestCase( 2, 1, ExpectedResult = 3, TestName = "Coordinate: (2,1) = 3" )]
        [TestCase( 1, 0, ExpectedResult = 4, TestName = "Coordinate: (1,0) = 4" )]
        public int GetAt_GivenCoordinate_ShouldReturnExpectedValue(int x, int y) {
            // Arrange
            int[,] state = {
                {1, 4, 0},
                {0, 2, 3}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act & Assert
            return board.GetAt(x, y).type;
        }

        [Test]
        public void GetAt_GivenCoordinatesOutsideBounds_ShouldThrowException() {
            // Arrange
            int[,] state = {
                {1, 0, 0},
                {0, 0, 0}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act - Assert
            Assert.Throws<IndexOutOfRangeException>(() => board.GetAt(x: 42, y: 0));
        }
        
        [TestCase( 1, 1, ExpectedResult = new[] { 2, 3, 5, 7, 3, 2 } )]
        [TestCase( 0, 0, ExpectedResult = new[] { 2, 5 } )]
        [TestCase( 3, 1, ExpectedResult = new[] { 4, 7, 11 } )]
        [TestCase( 3, 2, ExpectedResult = new[] { 7, 8, 2, 10, 12} )]
        [TestCase( 2, 2, ExpectedResult = new[] { 6, 7, 3, 11, 1, 10 } )]
        public int[] GetNeighbors_GivenCoordinate_ShouldReturnNeighboringValues(int x, int y) {
            // Arrange
            int[,] state = {
                {1, 2, 3, 4},
                  {5, 6, 7, 8},
                {9, 3, 2, 11},
                  {4, 1, 10, 12}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            Debug.Log("GetAt: " + board.GetAt(x,y));
            // Act & Assert
            return GetTypesFromPieces(board.GetNeighbors(x, y, RowHelper.IsRow(y)));
        }

        [Test]
        public void GetConnected_GivenAllNeighborsIsPieceType_ShouldReturnNeighborCells() {
            // Arrange
            int[,] state = {
                {1, 3, 1},
                {2, 3, 2},
                {1, 3, 1}
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act
            var connectedNeighbors = board.GetConnected(x: 1, y: 1);
            
            // Assert
            Assert.That(GetTypesFromPieces(connectedNeighbors), Is.EqualTo(new[] { 3, 3, 3 }));
        }
        
        [Test]
        public void GetConnected_GivenAsymmetricCluster_ShouldReturnConnectionOfSameType() {
            // Arrange
            int[,] state = {
                {1, 1, 0, 0, 0, 0},
                  {0, 1, 1, 0, 0, 0},
                {0, 1, 1, 1, 0, 0},
                  {0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 1},
                  {0, 0, 0, 0, 1, 1},
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act
            var connectedNeighbors = board.GetConnected(x: 1, y: 1);
            
            // Assert
            Assert.That(connectedNeighbors.Count, Is.EqualTo(8));
        }
        
        [Test]
        public void GetConnected_GivenLoopedCluster_ShouldReturnConnectionOfSameType() {
            // Arrange
            int[,] state = {
                {0, 0, 0, 0, 0, 0},
                  {0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 0},
                  {0, 1, 0, 1, 0, 0}, 
                {0, 1, 1, 1, 0, 0},
                  {0, 0, 0, 0, 1, 1},
            };
            var board = Board.Create(state, new PieceSpawnerFake(1));
            
            // Act
            var cluster = board.GetConnected(x: 1, y: 1);
            
            // Assert
            Assert.That(cluster.Count, Is.EqualTo(10));
        }
        
        [Test]
        public void Board_GivenDefinition_StateArrayHasSameOrientation() {
            // Arrange
            int[,] state = {
                {6, 1, 1, 1, 1 },
                {6, 2, 3, 4, 5 },
            };
            
            //Act            
            var board = Board.Create(state, new PieceSpawnerFake(42));
            
            //Assert
            Assert.That(board.GetAt(0, 0).type,Is.EqualTo(6));
            Assert.That(board.GetAt(4, 1).type,Is.EqualTo(5));
        }
        
        [Test]
		public void MovePieces_GivenSelectedPieceInAsymmetricalCluster_MovePiecesDown() {
			// Arrange
			int[,] state = {
				{0, 4, 4, 4, 4},
				{0, 2, 3, 1, 4},
				{0, 4, 4, 4, 0},
				{0, 0, 4, 0, 0}
			};
			var randomSpawner = new PieceSpawnerFake(42);
			var board = Board.Create(state, randomSpawner);
			
			// Act
			board.MovePieceseDown(new ResolveResult());

			// Assert
			int[,] expected = {
				{0, 0, 0, 0, 0},
				{0,  4, 4,  4, 4},
				{0,  2,  3,  1, 4},
				{0,  4,  4,  4, 0}
			};

			var result = board.GetBoardStateAsArrayWithTypes();
			Assert.That(result, Is.EqualTo(expected));
        }

        [TestCase( 1, 1, ExpectedResult = 4 )]
        [TestCase( 2, 2, ExpectedResult = 5 )]
        [TestCase( 3, 1, ExpectedResult = 2 )]
        public int GetEmptyNeighbors_GivenSelectedPiece_ShouldReturnEmptyCount(int x, int y) {
            // Arrange
            int[,] state = {
                {0, 0, 0, 0},
                  {0, 0, 4, 0},
                {0, 0, 4, 0},
                  {0, 0, 0, 0}
            };
            var randomSpawner = new PieceSpawnerFake(42);
            var board = Board.Create(state, randomSpawner);
            
            var v = board.GetEmptyNeighbors(x, y, RowHelper.IsRow(y));
            return v.Count;
        }
        
        private int[] GetTypesFromPieces(IPiece[] pieces) {
            return pieces.Select(p => p.type).ToArray();
        }
        
        private IPiece[] GetPiecesFromPosition(Board board, List<Vector2Int> position)
        {
            var pieces = new IPiece[position.Count];
            for (var i = 0; i < position.Count; i++)
            {
                pieces[i] = board.GetAt(position[i].x, position[i].y);
            }
            return pieces;
        }
        
        private int[] GetTypesFromPieces(List<IPiece> pieces) {
            return pieces.Select(p => p.type).ToArray();
        }
        
        private int[] GetSortedTypesFromPieces(List<Piece> pieces) {
            var list = pieces.Select(p => p.type).ToList();
            list.Sort();
            return list.ToArray();
        }
        
        [TestCase( 0, 0, ExpectedResult = 2, TestName = "Coordinate: (0,0) = 2" )]
        [TestCase( 1, 1, ExpectedResult = 2, TestName = "Coordinate: (1,1) = 2" )]
        [TestCase( 3, 3, ExpectedResult = 4, TestName = "Coordinate: (3,3) = 4" )]
        [TestCase( 4, 5, ExpectedResult = 6, TestName = "Coordinate: (4,5) = 6" )]
        public int GetConnected_GivenDefinition_StateArray_ShouldReturnConnectionCount(int x, int y) {
            // Arrange
            int[,] state = {
                {3, 3, 1, 2, 3, 3},
                  {2, 2, 1, 2, 3, 3},
                {1, 1, 6, 6, 4, 4},
                  {2, 2, 6, 6, 1, 1},
                {1, 1, 2, 2, 1, 1},
                  {1, 1, 2, 2, 1, 1},
            };
            
            var randomSpawner = new PieceSpawnerFake(42);
            var board = Board.Create(state, randomSpawner);
          
            // Act
            var cluster = board.GetConnected(x, y);
            
            // Assert
            return cluster.Count;
        }
        
        [TestCase(ExpectedResult = 7)]
        public int GetUnConnected_GivenDefinition_StateArray_ShouldReturnConnectionCount() {
            // Arrange
            int[,] state = {
                {3, 3, 0, 2, 3, 3},
                  {2, 2, 0, 2, 3, 3},
                {1, 0, 0, 6, 4, 4},
                  {0, 2, 0, 6, 1, 1},
                {1, 0, 2, 0, 0, 0},
                  {1, 1, 2, 0, 0, 1},
            };
            
            var randomSpawner = new PieceSpawnerFake(42);
            var board = Board.Create(state, randomSpawner);
          
            // Act
            var cluster = board.GetUnConnected();
            
            // Assert
            return cluster.Count;
        }
        
        [TestCase(ExpectedResult = 0)]
        public int GetUnConnected_GivenDefinition_StateArray_ShouldReturnConnectionCount2() {
            // Arrange
            int[,] state = {
                {1,3,4,3,1,1},
                  {0,2,0,0,1,0},
                {0,0,0,0,0,1},
            };
            
            var randomSpawner = new PieceSpawnerFake(42);
            var board = Board.Create(state, randomSpawner);
          
            // Act
            var cluster = board.GetUnConnected();
            
            // Assert
            return cluster.Count;
        }

        [TestCase( 2, 1, ExpectedResult = new []{0, 0} )]
        [TestCase( 3, 1, ExpectedResult = new []{3, 0} )]
        [TestCase( 4, 4, ExpectedResult = new []{4, 3} )]
      //  [TestCase( 4, 5, ExpectedResult = 6, TestName = "Coordinate: (4,5) = 6" )]
        public int[] GetBestConnectedPiece_GivenDefinition_StateArray_ShouldReturnPosition(int x, int y) {
            // Arrange
            int[,] state = {
                {1, 1, 1, 2, 3, 3},
                  {2, 2, 1, 2, 3, 3},
                {1, 1, 6, 6, 4, 4},
                  {2, 2, 6, 6, 1, 1},
                {1, 1, 2, 0, 1, 1},
                  {1, 1, 2, 0, 1, 1},
            };
            
            var randomSpawner = new PieceSpawnerFake(42);
            var board = Board.Create(state, randomSpawner);

            var piece = board.GetBestConnectedPiece(board.GetConnected(x, y));

            // Assert
            return new [] {piece.pos.x, piece.pos.y};
        }
    }
}