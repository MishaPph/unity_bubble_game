﻿using UnityEngine;
using UnityEditor;

public class ToolsEditor : MonoBehaviour {

	[MenuItem("Tools/Clear PlayerPref")]
	public static void OpenInit() {
		PlayerPrefs.DeleteAll();
	}
	#region ScreenShot
	[MenuItem("Tools/ScreenCapture %g", false)]
	private static void ScreenCaptureFile()
	{
		var fileName =  System.IO.Path.Combine(Application.persistentDataPath, "ScreenCapture_" + Time.fixedTime + ".png");
		ScreenCapture.CaptureScreenshot(fileName);
		EditorUtility.RevealInFinder(Application.persistentDataPath +"/");
	}
	#endregion
}
