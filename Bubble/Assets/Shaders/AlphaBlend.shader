﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UI/AlphaBlend"
{
Properties {
 _MainTex ("Sprite Texture", 2D) = "white" { }
 _Color ("Tint", Color) = (1,1,1,1)
 _StencilComp ("Stencil Comparison", Float) = 8
 _Stencil ("Stencil ID", Float) = 0
 _StencilOp ("Stencil Operation", Float) = 0
 _StencilWriteMask ("Stencil Write Mask", Float) = 255
 _StencilReadMask ("Stencil Read Mask", Float) = 255
 _ColorMask ("Color Mask", Float) = 15
 _Mult ("Mult", Float) = 0
}
 SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 100
    
    ZWrite Off
    Blend SrcAlpha OneMinusSrcAlpha 
    
    Pass {  
     	Stencil {

             ZFail decrWrap
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]

            }
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
             #pragma fragmentoption ARB_precision_hint_fastest
        
            struct appdata_t {
                fixed4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                fixed4 color : COLOR0;
            };

            struct v2f {
                fixed4 vertex : SV_POSITION;
                half2 texcoord : TEXCOORD0;
                half4 color : COLOR0;
            };

            sampler2D _MainTex;
   
            fixed4 _Color;
            fixed4 _MainTex_ST;
            float _Mult;
        
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.color = v.color*_Mult;
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.texcoord) * _Color * i.color;
                return col;
            }
        ENDCG
    }
}

}