﻿public interface IGameController
{
    void Restart();
    void Menu();
    void NextLevel();
    void Levels();
}