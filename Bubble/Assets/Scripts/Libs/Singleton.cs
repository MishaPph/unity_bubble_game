using UnityEngine;

namespace Libs
{
	public class Singleton<T> : MonoBehaviour where T : Singleton<T>
	{
		private static T _instance;
		
		private static readonly object _lock = new object();

		public static T Instance
		{
			get
			{
				lock(_lock)
				{
					if (_instance != null) return _instance;
					
					_instance = Find();

					if (_instance == null) 
						_instance = LoadFromResources();
				
					if (_instance == null)
						_instance = Create();
					return _instance;
				}
			}
		}

		private static T Find()
		{
			var ins = (T) FindObjectOfType(typeof(T));
			if (ins != null)
			{
				DontDestroyOnLoad(ins);
				return ins;
			}
			if (FindObjectsOfType(typeof(T)).Length > 1)
				Debug.LogError("[Singleton] Something went really wrong " +
			      " - there should never be more than 1 singleton!" +
			      " Reopening the scene might fix it.");
			return null;
		}
	
		private static T LoadFromResources()
		{
			var strName = typeof(T).Name;
			var prefab = Resources.Load(strName);
			if (prefab == null)
			{
				//Log.w(strName);
				return null;
			}
			var singleton = Instantiate(prefab);
			DontDestroyOnLoad(singleton);
			singleton.name = strName;
			return ((GameObject) singleton).GetComponent<T>();	
		}

		protected void Awake()
		{
			DontDestroyOnLoad(gameObject);
		}

		private static T Create()
		{
			var singleton = new GameObject(typeof(T).ToString());
			DontDestroyOnLoad(singleton);
			return singleton.AddComponent<T>();
		}
	}
}
