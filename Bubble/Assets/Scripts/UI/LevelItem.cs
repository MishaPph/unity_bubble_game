using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LevelItem : MonoBehaviour
    {
        private static readonly Color ActiveColor = new Color(0.528817f, 0.243483f, 0.879f);
        private static readonly Color LockedColor = new Color(0.23f, 0.13f, 0.42f);
        
        [SerializeField] private TextMeshProUGUI _number;
        [SerializeField] private Button _button;
        [SerializeField] private Image _lock;
        [SerializeField] private Image _complete;
        [SerializeField] private TextMeshProUGUI _score;
        
        private Action<int> _chooseLevel;
        private int _level;

        private void Awake()
        {
            _button.onClick.AddListener(OnClick);
        }

        public void Set(int level, bool locked, bool completed, bool showscore, Action<int> onClick)
        {
            _level = level;
            _number.text = level.ToString();
            _chooseLevel = onClick;
            _button.interactable = !locked;
            _number.color = locked?LockedColor:ActiveColor;
            _lock.gameObject.SetActive(locked);
            _complete.gameObject.SetActive(completed);
            _score.gameObject.SetActive(showscore);
            _score.text = TextHelper.ScoreToString(UserData.GetLevelScore(level));
        }

        private void OnClick()
        {
            _chooseLevel?.Invoke(_level);
        }
    }
}
