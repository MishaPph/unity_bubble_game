using System;
using Audio;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SoundToggle : MonoBehaviour
    {
        [SerializeField] private Sprite _soundOn;
        [SerializeField] private Sprite _soundOff;
        [SerializeField] private Image _soundIcon;

        private void Awake()
        {
            _soundIcon.sprite = SoundManager.Enable ? _soundOn : _soundOff;
        }

        public void Toggle()
        {
            SoundManager.Enable = !SoundManager.Enable;
            _soundIcon.sprite = SoundManager.Enable ? _soundOn : _soundOff;
        }
    }
}
