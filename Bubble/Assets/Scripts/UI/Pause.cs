﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UI
{
    public class Pause : MonoBehaviour
    {
        private const float HidePanelX = 1000;
        [SerializeField] private RectTransform _panel;
        
        private Vector2 _startPos;

        private IGameController _gameController;
        public bool Visible => gameObject.activeSelf;
        
        private void Awake()
        {
            _startPos = _panel.anchoredPosition;
        }

        public void Show(IGameController gameController)
        {
            _gameController = gameController;
            Time.timeScale = 0;
            gameObject.SetActive(true);
            _panel.anchoredPosition = new Vector2(_startPos.x - HidePanelX, _startPos.y);
            _panel.DOAnchorPos(_startPos, 0.3f).SetEase(Ease.OutBack).SetUpdate(true);
        }

        public void Hide()
        {
            var end = new Vector2(_startPos.x - HidePanelX, _startPos.y);
            _panel.DOAnchorPos(end, 0.3f).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(Close);
        }
        
        public void Close()
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);
        }

        public void RateApp()
        {
            Application.OpenURL(Const.AppRateUrl);
        }
        
        public void Restart()
        {
            Close();
            _gameController.Restart();
        }
        
        public void Resume()
        {
            Close();
        }

        public void Levels()
        {
            Close();
            _gameController.Levels();
        }

        public void Exit()
        {
            Close();
            _gameController.Menu();
        }
    }
}