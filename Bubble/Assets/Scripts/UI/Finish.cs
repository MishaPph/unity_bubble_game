using DG.Tweening;
using Levels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Finish : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _level;
        [SerializeField] private TextMeshProUGUI _score;
        
        [SerializeField] private Button _next;
        [SerializeField] private Button _exit;
        [SerializeField] private RectTransform _panel;

        private IGameController _gameController;

        private void Awake()
        {
            _exit.onClick.AddListener(Exit);
            _next.onClick.AddListener(Next);
        }

        private void Exit()
        {
            Hide();
            _gameController.Menu();
        }

        private void Next()
        {
            Hide();
            _gameController.NextLevel();
        }

        public void Show(bool win, int level, int score, IGameController gameController)
        {
            _level.text = $"level: {level}";
            _score.text = $"score: {TextHelper.ScoreToString(score)}";
            var lastLevel = LevelManager.Instance.Count == level;
            
            if (win)
            {
                _panel.sizeDelta = new Vector2(_panel.sizeDelta.x, 768);
            }
            else if (lastLevel)
            { 
                _panel.sizeDelta = new Vector2(_panel.sizeDelta.x, 620);
            }
            else
            {
                _score.text = "";
                _panel.sizeDelta = new Vector2(_panel.sizeDelta.x, 520);
            }

            _next.gameObject.SetActive(!lastLevel && win);
            
            _gameController = gameController;
            gameObject.SetActive(true);
            _panel.localScale = Vector3.zero;
            _panel.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack).SetUpdate(true);
        }

        public void Restart()
        {
            Hide();
            _gameController.Restart();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        
        public void RateApp()
        {
            Application.OpenURL(Const.AppRateUrl);
        }
    }
}
