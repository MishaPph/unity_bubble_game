﻿namespace UI
{
    public static class TextHelper
    {
        public static string ScoreToString(int score)
        {
            return $"{score:###,###,###}";
        }
    }
}
