using System;
using Libs;
using UnityEngine;

namespace UI
{
    public class Dialog : Singleton<Dialog>
    {
        [SerializeField] private Pause _pause;

        public Pause Pause => _pause;
        
        [SerializeField] private Finish _finish;

        public Finish Finish => _finish;
        
        public void ChangePauseVisibility(IGameController gameController)
        {
            if (Pause.Visible)
            {
                Pause.Hide();
            } else
                Pause.Show(gameController);
        }
    }
}
