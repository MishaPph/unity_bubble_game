using System;
using System.Collections;
using System.Collections.Generic;
using Libs;
using UnityEngine;

namespace Audio
{
    public enum SoundId
    {
        FallHit,
        Destroy,
        Hit,
        Spawn,
        Click,
        Success,
        Label,
        Fail,
        HitSmall
    }
    public class SoundManager : Singleton<SoundManager>
    {
        [SerializeField] private List<AudioClip> _clips;
        
        [SerializeField] private AudioSource _audioSourcePrefab;
        public bool LogEnable = false;

        private readonly List<AudioSource> _sources = new List<AudioSource>();

        private float Volume => 1;

        public static bool Enable
        {
            get => AudioListener.volume > 0;
            set
            {
                PlayerPrefs.SetInt("SoundEnable", value?1:0);
                AudioListener.volume = value ? 1 : 0;
            }
        }

        private void Start()
        {
            Enable = PlayerPrefs.GetInt("SoundEnable", 1) == 1;
        }
        
        public void Pause()
        {
            foreach (var source in _sources) {
                source.Pause();
            }
        }
    
        public void Resume()
        {
            foreach (var source in _sources) {
                source.UnPause();
            }
        }
    
        public void Stop()
        {
            foreach (var source in _sources) {
                source.Stop();
            }
        }

        public AudioClip Get(SoundId soundId)
        {
            var clipName = soundId.ToString();
            return _clips.Find(s => s.name == clipName);
        }

        public AudioSource Play(SoundId soundId)
        {
            return Play(Get(soundId));
        }
        
        private void ChangeVolume(float volume)
        {
            foreach (var source in _sources) 
                source.volume = volume;
        }

        public AudioSource Play(AudioClip clip)
        {
            if (LogEnable)
            {
                Debug.Log("[SFXManager] Play2D " + clip.name);
            }
            var sound = Play(clip, 0);
            return sound;
        }
    
        private AudioSource Play(AudioClip clip, float spatialBlend)
        {
            if (clip == null) {
                Debug.LogError("Null clip");
                return null;
            }
            var s = GetFree();
            s.clip = clip;
            s.volume = Volume;
            s.spatialBlend = spatialBlend;
            s.loop = false;
            s.Play();
            return s;
        }

        public void Stop(AudioSource clip, float fadeTime = .1f, Action onComplete = null)
        {
            if (LogEnable)
            {
                Debug.Log("[SFXManager] Stop " + clip.name);
            }
            StartCoroutine(StopAudioClipCoroutine(clip, fadeTime, onComplete));
        }

        private IEnumerator StopAudioClipCoroutine(AudioSource clip, float fadeTime, Action onComplete)
        {
            var time = 0f;
            while (time < fadeTime)
            {
                time += Time.deltaTime;
                clip.volume = Mathf.Lerp(0, fadeTime, time / fadeTime);
                yield return new WaitForEndOfFrame();
            }
            clip.Stop();
            onComplete?.Invoke();        
        }
    
        private AudioSource GetFree()
        {
            foreach (var source in _sources)
            {
                if (source.isPlaying) continue;
                source.volume = 0;
                return source;
            }
            var audioSource = Instantiate(_audioSourcePrefab, transform);
            audioSource.volume = 0;
            audioSource.loop = false;
            audioSource.playOnAwake = false;
            _sources.Add(audioSource);
            return audioSource;
        }
    }
}