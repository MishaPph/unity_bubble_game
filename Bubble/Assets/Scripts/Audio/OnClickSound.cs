using UnityEngine;
using UnityEngine.EventSystems;

namespace Audio
{
    [RequireComponent(typeof(IPointerClickHandler))]
    public class OnClickSound : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            SoundManager.Instance.Play(SoundId.Click);
        }
    }
}
