using System.Collections.Generic;
using UnityEngine;

namespace Model {

	[CreateAssetMenu(menuName = "Scripts/PieceTypeDatabase")]
	public class PieceTypeDatabase : ScriptableObject {
		
		[SerializeField] private List<Color> colors;

		public Color GetColorForPieceType(int pieceType) {
			if (pieceType > 0 && pieceType < colors.Count) { 
				return colors[pieceType - 1];
			}
			return Color.clear;
		}

		public Color GetColorForPiece(IPiece piece)
		{
			return GetColorForPieceType(piece.type);
		}
	}
}