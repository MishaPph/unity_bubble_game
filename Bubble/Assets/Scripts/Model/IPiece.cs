﻿namespace Model
{
    public interface IPiece
    {
        int Value { get; }
        int type { get; set; }

        bool IsSpecial { get; }
        
        SpecialId SpecialId { get; }

        string GetStringValue();
    }
}