namespace Model {

	public enum ChangeState
	{
		None,
		Moved,
		Created,
		Removed,
		Dropped,
		Changed
	}
	public class ChangeInfo {
		public ChangeState State { get; set; }
		public BoardPos FromPos { get; set; }
		public BoardPos ToPos { get; set; }
	}
	
}