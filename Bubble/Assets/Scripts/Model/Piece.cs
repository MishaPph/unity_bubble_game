﻿using UnityEngine;

namespace Model
{
    public class Piece: IPiece
    {
        public static int Max = 0;
        public static int Base = 2;
        
        private int _type = 0;
        public int type
        {
            get => _type;
            set => _type = value;
        }

        public bool IsSpecial => false;
        public SpecialId SpecialId => SpecialId.None;
        
        public string GetStringValue()
        {
            return Value > 10000 ? $"{Value / 10000}K" : Value.ToString();
        }

        public Piece(int t)
        {
            type = t;
        }
        
        public int Value => (int)Mathf.Pow(Base, Mathf.Min(Max, type));

        public override string ToString() {
            return $"(type:{type})";
        }
    }
    
    public class SpecialPiece: IPiece
    {
        private int _type = 0;
        public int type
        {
            get => _type;
            set => _type = value;
        }

        public bool IsSpecial => true;
        public SpecialId SpecialId { get; private set; }
        
        public string GetStringValue()
        {
            return SpecialId.GetName();
        }

        public SpecialPiece(int t)
        {
            type = t;
            SpecialId = (t > Const.SpecialOffset)? (SpecialId)(t):SpecialId.None;
        }
        
        public int Value => (int)1;

        public override string ToString() {
            return $"(type:{type})";
        }
    }
}