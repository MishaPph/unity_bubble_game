namespace Model {
	public interface IPieceSpawner {
		IPiece CreateBasicPiece();
		IPiece CreateTopPiece();
		IPiece CreateUserPiece(bool lastPhase, int minTypeOnBoard);
		int Max { get; }
		int MaxValue { get; }
		IPiece CreatePiece(int type);
	}

	public static class PieceSpawnerExt
	{
		public static bool IsMax(this IPieceSpawner pieceSpawner, IPiece piece)
		{
			return piece.type >= pieceSpawner.Max || piece.SpecialId == SpecialId.Bomb;
		}
	}
}