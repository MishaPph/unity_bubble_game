﻿namespace Model
{
    public enum SpecialId : int
    {
        None,
        Any = Const.SpecialOffset + 1,
        Bomb = Const.SpecialOffset + 2
    }

    public static class SpecialIdExt
    {
        public static string GetName(this SpecialId id)
        {
            switch (id)
            {
                case SpecialId.Any:
                    return "A";
                case SpecialId.Bomb:
                    return "B";
            }
            return "None";
        }
    }
}