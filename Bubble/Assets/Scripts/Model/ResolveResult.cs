using System.Collections.Generic;

namespace Model {

	public class ResolveResult {
		public readonly List<Dictionary<IPiece, ChangeInfo>> changes = new List<Dictionary<IPiece, ChangeInfo>>();
		
		public readonly List<ChangeInfo> movement = new List<ChangeInfo>();
		public readonly List<ChangeInfo> dropped = new List<ChangeInfo>();
		
		public readonly  Dictionary<IPiece, ChangeInfo> created = new  Dictionary<IPiece, ChangeInfo>();
		public (IPiece, BoardPos) AddedPiece;
	}

}