using System.Collections.Generic;
using Levels;
using UnityEngine;

namespace Model {
	
	public class PieceSpawner : IPieceSpawner
	{
		private readonly LevelData _leveData;

		private int _amountSpawnedUserPieces;

		private readonly List<IPieceCreator> _topPieceCreators = new List<IPieceCreator>();
		
		private readonly List<IPieceCreator> _userPieceCreators = new List<IPieceCreator>();
		public IPiece CreateUserPiece(bool lastPhase, int minTypeOnBoard)
		{
			_amountSpawnedUserPieces++;
			foreach (var creator in _userPieceCreators)
			{
				if (creator.TryCreate(lastPhase, minTypeOnBoard, _amountSpawnedUserPieces, out var p))
				{
					return p;
				}
			}
			return CreateBasicPiece();
		}
		public int Max { get; }
		public int MaxValue => (int)Mathf.Pow(_leveData.PowBase, Max);
		public PieceSpawner(LevelData leveData)
		{
			_leveData = leveData;
			
			_userPieceCreators.Add(new SpecialPieceCreator(leveData));
			_userPieceCreators.Add(new PredefinedPieceCreator(BoardDataHelper.ToPiecesQueue(leveData.UserPieceQueue)));
			if (_leveData.FastFinish)
				_userPieceCreators.Add(new FastFinishPieceCreator(leveData));
			else
				_userPieceCreators.Add(new LastPhasePieceCreator(leveData));
			
			var topQ =  BoardDataHelper.ToPiecesQueueWithTrim(leveData.AdditionalData);
			if(topQ.Count > 0)
				_topPieceCreators.Add(new PredefinedPieceCreator(topQ));
			
			Max = Piece.Max = _leveData.Max;
			Piece.Base = _leveData.PowBase;
		}
		
		public IPiece CreateTopPiece()
		{
			foreach (var creator in _topPieceCreators)
			{
				if (creator.TryCreate(false, 0, _amountSpawnedUserPieces, out var p))
				{
					return p;
				}
			}
			return CreateBasicPiece();
		}
		
		public IPiece CreateBasicPiece()
		{
			return CreatePiece( Random.Range(_leveData.StartedValue, _leveData.EndGValue(_amountSpawnedUserPieces)));
		}
		public IPiece CreatePiece(int type)
		{
			if (type > Const.SpecialOffset)
				return new SpecialPiece(type);
			return new Piece(type);
		}
		
	}
}