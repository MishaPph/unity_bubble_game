using System;

namespace Model {

	public struct BoardPos {
		public int x;
		public int y;

		public BoardPos(int x,int y) {
			this.x = x;
			this.y = y;
		}

		public override string ToString()
		{
			return $"{x} {y}";
		}

		public static float Distance(BoardPos a, BoardPos b)
		{
			float num1 = a.x - b.x;
			float num2 = a.y - b.y;
			return (float) Math.Sqrt(num1 * num1 + num2 * num2);
		}
	}
}