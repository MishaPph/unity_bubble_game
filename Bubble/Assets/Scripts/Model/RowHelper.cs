﻿namespace Model
{
    public static class RowHelper
    {
        public static int RowIndex = 0;
        public static bool IsRow(int value)
        {
            return (RowIndex + value) % 2 == 0;
        }
    }
}