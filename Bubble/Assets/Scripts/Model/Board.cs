using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Model {
	public class Board {
        
        private IPiece[,] boardState;
        private readonly IPieceSpawner pieceSpawner;

        public static Board Create(int[,] definition, IPieceSpawner pieceSpawner) {
            return new Board(definition, pieceSpawner);
        }

        public int CountAddTopRow = 0;
        
        public int Width => boardState.GetLength(0);

        public int Height => boardState.GetLength(1);

        public Board(int[,] definition, IPieceSpawner pieceSpawner) {
            
            this.pieceSpawner = pieceSpawner;
            RowHelper.RowIndex = 0;
            var transposed = ArrayUtility.TransposeArray(definition);
            CreatePieces(transposed);
        }

        private void CreatePieces(int[,] array) {
            
            var defWidth = array.GetLength(0);
            var defHeight = array.GetLength(1);
            
            boardState = new IPiece[defWidth,defHeight];
            
            for (var y = 0; y < defHeight; y++) {
                for (var x = 0; x < defWidth; x++) {
	                if(array[x,y] != 0)
		                CreatePiece(pieceSpawner.CreatePiece(array[x,y]), x, y);
                }
            }
        }
        
        public IEnumerable<PiecePosition> IteratePieces() {
	        for (var y = 0; y < Height; y++) {
		        for (var x = 0; x < Width; x++) {
			        if(boardState[x, y] == null)
				        continue;
			        yield return new PiecePosition() {
				        piece = boardState[x, y],
				        pos = new BoardPos(x, y)
			        };
		        }
	        }
        }
        
        public int GetMinPieces()
        {
	        var min = 100;
	        for (var y = 0; y < Height; y++) {
		        for (var x = 0; x < Width; x++) {
			        if(boardState[x, y] == null)
				        continue;
			        min = Mathf.Min(min, boardState[x, y].type);
		        }
	        }
	        return min;
        }
        
        public IPiece CreatePiece(IPiece piece, int x, int y) {
	        boardState[x, y] = piece;
            return piece;
        }
        
        public int[,] GetBoardStateAsArrayWithTypes() {
	        var result = new int[Width, Height];
            for (int x = 0; x < Width; x++) {
                for (int y = 0; y < Height; y++) {
                    var p = boardState[x,y];
                    result[x, y] = p?.type ?? 0;
                }
            }
            return ArrayUtility.TransposeArray(result);
        }

        public IPiece GetAt(int x, int y) {
            return boardState[x, y];
        }

        public bool Clear() {
	        for (var x = 0; x < Width; x++) {
		        for (var y = 0; y < Height; y++)
		        {
			        if (boardState[x, y] != null)
				        return false;
		        }
	        }
	        return true;
        }

        public bool FirstRowEmpty()
        {
	        for (var x = 0; x < Width; x++) {
		        if (boardState[x, 0] != null)
			        return false;
	        }
	        return true;
        }
        
        public bool LastRowNotEmpty()
        {
	        var y = Height - 1;
	        for (var x = 0; x < Width; x++) {
		        if (boardState[x, y] != null)
			        return true;
	        }
	        return false;
        }
        
        public void MovePiece(int fromX, int fromY, int toX, int toY) {
            boardState[toX, toY] = boardState[fromX, fromY];
            boardState[fromX, fromY] = null;
        }
        
        public bool IsWithinBounds(int x, int y) {
            if (x < Width && y < Height && x >= 0 && y >= 0) {
                return true;
            }
            return false;
        } 
        
        public void RemovePieceAt(int x, int y) {
	        boardState[x, y] = null;
        }
        
        public bool TryGetPiecePos(IPiece piece, out int px, out int py) {
               for (var y = 0; y < Height; y++) {
                   for (var x = 0; x < Width; x++) {
                       if (boardState[x, y] == piece) {
                           px = x;
                           py = y;
                           return true;
                       }
                   }
               }
               px = -1;
               py = -1;
               return false;
        }
        
        public List<IPiece> GetConnected(int x, int y) {
            var start = GetAt(x, y);
            return SearchForConnected(start, new List<IPiece>(), (p)=>p.type == start.type);
        }

        private List<IPiece> SearchForConnected(IPiece piece, List<IPiece> searched, Func<IPiece, bool> predict) {
	        if (!TryGetPiecePos(piece, out var x, out var y)) {
                return searched;
            }

            searched.Add(piece);
            var neighbors = GetNeighbors(x,y, RowHelper.IsRow(y));
            
            if (neighbors.Count == 0) {
                return searched;
            }

            for (var i = 0; i < neighbors.Count; i++) {
	            var neighbor = neighbors[i];
                if (!searched.Contains(neighbor) && predict(neighbor)) {
                    SearchForConnected(neighbor, searched, predict);
                }
            }
            return searched;
        }

        public List<IPiece> GetUnConnected()
        {
	        var l = new List<IPiece>();
	        for (var x = 0; x < Width; x++)
	        {
		        var piece = GetAt(x, 0);
		        if(piece != null)
			        SearchForConnected(piece, l, (p) => true);
	        }
	        var unList = new List<IPiece>();
	        for (var y = 0; y < Height; y++) {
		        for (var x = 0; x < Width; x++) {
			        if(boardState[x, y] == null)
				        continue;
			        if (!l.Contains(boardState[x, y]))
			        {
				        unList.Add(boardState[x, y]);
			        }
		        }
	        }
	        return unList;
        }

        public List<IPiece> GetNeighbors(int x, int y) => GetNeighbors(x, y, RowHelper.IsRow(y));
        
        public List<IPiece> GetNeighbors(int x, int y, bool offset) {

            var neighbors = new List<IPiece>(6);
            
            if(offset)
	            neighbors = AddNeighbor(x - 1, y - 1, neighbors); // Left Top

            neighbors = AddNeighbor(x, y - 1, neighbors); // Top
            
            if(!offset)
				neighbors = AddNeighbor(x + 1, y - 1, neighbors); // Right Top
            
            neighbors = AddNeighbor(x - 1, y, neighbors); // Left
            neighbors = AddNeighbor(x + 1, y, neighbors); // Right
            
            if(offset)
	            neighbors = AddNeighbor(x - 1, y + 1, neighbors); // Left Bottom
            
            neighbors = AddNeighbor(x, y + 1, neighbors); // Bottom
            
            if(!offset)
				neighbors = AddNeighbor(x + 1, y + 1, neighbors); // Right Bottom
            
            
            return neighbors;
        }
        
        public List<Vector2Int> GetEmptyNeighbors(int x, int y, bool offset) {
	        var neighbors = new List<Vector2Int>(7);
	        
	        if (offset)
	        {
		        AddIfCanPlaceInThatPosition(x - 1, y - 1, neighbors);
	        }

	        AddIfCanPlaceInThatPosition(x, y - 1, neighbors) ; // Top

	        if (!offset)
	        {
		        AddIfCanPlaceInThatPosition(x + 1, y - 1, neighbors); // Right Top
	        }

	        AddIfCanPlaceInThatPosition(x - 1, y, neighbors); // Left

	        AddIfCanPlaceInThatPosition(x + 1, y, neighbors); // Right
	        
	        if (offset)
	        {
		        AddIfCanPlaceInThatPosition(x - 1, y + 1, neighbors); //Left  Bottom 
	        }

	        AddIfCanPlaceInThatPosition(x, y + 1, neighbors); // Bottom
	        
	        if (!offset)
	        {
		        AddIfCanPlaceInThatPosition(x + 1, y + 1, neighbors); // Right Bottom
	        }
	        return neighbors;
        }
        
        private void AddIfCanPlaceInThatPosition(int x, int y, List<Vector2Int> neighbors) {
	        if (CanPlaceInThatPosition(x, y))
	        {
		        neighbors.Add(new Vector2Int(x, y));
	        }
        }
        
        private bool CanPlaceInThatPosition(int x, int y) {
	        if (!IsWithinBounds(x, y)) return false;
	        return GetAt(x,y) == null;
        }
        
        private List<IPiece> AddNeighbor(int x, int y, List<IPiece> neighbors) {
            if (!IsWithinBounds(x, y)) return neighbors;
            if (GetAt(x, y) == null)
	            return neighbors;
            neighbors.Add(GetAt(x,y));
            return neighbors;
        }

		private Dictionary<IPiece, BoardPos> RemovePieces(List<IPiece> connections) {
			var result = new Dictionary<IPiece, BoardPos>();
			foreach (var piece in connections) {
				if(TryGetPiecePos(piece, out var x, out var y)){ 
					RemovePieceAt(x,y);
					result.Add(piece, new BoardPos(x, y));
				}
			}
			return result;
		}
		
		private void CreatePiecesAtTop(ResolveResult resolveResult)
		{
			const int y = 0;
			for (var x = 0; x < Width; x++) {
				var piece = CreatePiece(pieceSpawner.CreateTopPiece(), x,y);
				resolveResult.created[piece] = new ChangeInfo{
					State = ChangeState.Created,
					ToPos = new BoardPos(x,y),
					FromPos = new BoardPos(x,y-1)
				};
			}
		}

		public void MovePieceseDown(ResolveResult resolveResult) {
			for (var y = Height - 1; y >= 1; y--) {
				for (var x = 0; x < Width; x++) {
					
					var fromY = y - 1;
					var pieceToMove = GetAt(x, y - 1);
					if (pieceToMove == null) {
						continue;
					}
					var fromX = x;
					MovePiece(fromX, fromY, x, y);
					
					resolveResult.movement.Add( new ChangeInfo
					{
						FromPos = new BoardPos(fromX, fromY),
						ToPos = new BoardPos(x,y),
						State = ChangeState.Moved,
					});
				}
			}
		}

		public ResolveResult PutPiece(int x, int y, IPiece piece)
		{
			var result = new ResolveResult();
			if (piece.IsSpecial)
			{
				if (piece.SpecialId == SpecialId.Any)
				{
					var pieces = GetNeighbors(x, y);
					if (pieces.Count == 0)
					{
						var max = 0;
						foreach (var p in IteratePieces())
						{
							max = Math.Max(p.piece.type, max);
						}
						piece = pieceSpawner.CreatePiece(max);
					}
					else
					{
						var p = pieces[UnityEngine.Random.Range(0, pieces.Count - 1)];
						piece = pieceSpawner.CreatePiece(p.type);
					}
				} 
			}
			
			CreatePiece(piece, x, y);
			result.AddedPiece = (piece, new BoardPos(x, y));
			
			ResolveAdding(x, y, result);

			RemoveUnconnected(result);

			if (CountAddTopRow > 0)
			{
				MovePieceseDown(result);
				CreatePiecesAtTop(result);
			}
			CountAddTopRow--;
			
			return result;
		}

		public void RemoveUnconnected(ResolveResult result)
		{
			var unconnected = GetUnConnected();
			if (unconnected.Count > 0)
			{
				foreach (var p in unconnected)
				{
					if (TryGetPiecePos(p, out var x1, out var y1))
					{
						result.dropped.Add(new ChangeInfo
						{
							FromPos = new BoardPos(x1, y1),
							State = ChangeState.Dropped
						});
						RemovePieceAt(x1, y1);
					}
				}
			}
		}

		public List<PiecePosition> ConvertToPiecePosition(List<IPiece> pieces)
		{
			var list = new List<PiecePosition>(pieces.Count);
			for (var y = 0; y < Height; y++) {
				for (var x = 0; x < Width; x++) {
					if(boardState[x, y] == null)
						continue;
					var piece = boardState[x, y];
					if (pieces.Contains(piece))
					{
						list.Add(new PiecePosition {
							piece = boardState[x, y],
							pos = new BoardPos(x, y)
						});
					}
				}
			}
			return list;
		}
		
		public PiecePosition GetBestConnectedPiece(List<IPiece> pieces)
		{
			var l = Mathf.Min(pieceSpawner.Max, pieces[0].type + pieces.Count - 1);
			var list = ConvertToPiecePosition(pieces);
			TryGetPiecePos(pieces[0], out var x, out var y);
			var startPosition = new BoardPos(x, y);
			list.Sort(Sort);

			int Sort(PiecePosition a, PiecePosition b)
			{
				return BoardPos.Distance(b.pos, startPosition).CompareTo(BoardPos.Distance(a.pos, startPosition));
			}

			foreach (var pp in list)
			{
				if (GetNeighbors(pp.pos.x, pp.pos.y, RowHelper.IsRow(pp.pos.y)).Any(piece => piece.type == l))
				{
					return pp;
				}
			}
			return list[0];
		}

		private void ResolveAdding(int x, int y, ResolveResult result)
		{
			var piece = GetAt(x, y);
			var dict = new Dictionary<IPiece, ChangeInfo>();
			if (pieceSpawner.IsMax(piece))
			{
				foreach (var p in GetNeighbors(x, y, RowHelper.IsRow(y)))
				{
					RemovePiece(dict, p);
				}
				RemovePiece(dict, piece);
				result.changes.Add(dict);
				return;
			}

			var connections = GetConnected(x, y);
			if (connections.Count > 1)
			{
				var bestPiece = GetBestConnectedPiece(connections);
				for (var i = 0; i < connections.Count; i++)
				{
					if (connections[i] != bestPiece.piece)
					{
						RemovePiece(dict, connections[i]);
					}
				}
				
				dict[bestPiece.piece] = new ChangeInfo
				{
					FromPos = bestPiece.pos,
					State = ChangeState.Changed
				};
				boardState[bestPiece.pos.x, bestPiece.pos.y].type += connections.Count - 1;
				
				result.changes.Add(dict);
				
				ResolveAdding(bestPiece.pos.x, bestPiece.pos.y, result);
			}
		}

		private void RemovePiece(Dictionary<IPiece, ChangeInfo> result, IPiece p)
		{
			if (!TryGetPiecePos(p, out var x1, out var y1)) return;
			result[p] = new ChangeInfo
			{
				FromPos = new BoardPos(x1, y1),
				State = ChangeState.Removed
			};
			RemovePieceAt(x1, y1);
		}
    }
}