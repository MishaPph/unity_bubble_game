using System.Collections.Generic;
using UnityEngine;

namespace Model {
	public static class ArrayUtility {
		public static int[,] TransposeArray(int[,] array) {
			var transposed = new int[array.GetLength(1), array.GetLength(0)];

			for (int y = 0; y < array.GetLength(1); y++) {
				for (int x = 0; x < array.GetLength(0); x++) {
					transposed[y, x] = array[x, y];
				}        
                
			}

			return transposed;
		}
		
		public static T[,] TransposeArray<T>(T[,] array) {
			var transposed = new T[array.GetLength(1), array.GetLength(0)];

			for (var y = 0; y < array.GetLength(1); y++) {
				for (var x = 0; x < array.GetLength(0); x++) {
					transposed[y, x] = array[x, y];
				}
			}
			return transposed;
		}
		
		public static Vector3[] CastListToPath(IReadOnlyList<Vector2> points, out float length)
		{
			var wayPoint = new Vector3[points.Count];
			length = 0;
			for (var i = 0; i < points.Count; i++)
			{
				wayPoint[i] = points[i];
				if (i > 0)
				{
					length += Vector2.Distance(points[i], points[i - 1]);
				}
			}
			return wayPoint;
		}
	}

}