﻿using System.Collections.Generic;
using Levels;
using UnityEngine;
namespace Model
{
    public interface IPieceCreator
    {
        bool TryCreate(bool lastPhase, int minTypeOnBoard, int amountSpawned, out IPiece piece);
    }
    
    public class SpecialPieceCreator : IPieceCreator
    {
       	private readonly LevelData _leveData;
       	public SpecialPieceCreator(LevelData leveData) => _leveData = leveData;
       	public bool TryCreate(bool lastPhase, int minTypeOnBoard, int amountSpawned, out IPiece piece)
       	{
       		foreach (var v in _leveData.Specials)
       		{
       			if(v.After > amountSpawned)
       				continue;
       			var r = Random.Range(0, 100);
       			if (v.Percent < r) continue;
       			piece = new SpecialPiece((int)v.Id);
       			return true;
       		}
       		piece = null;
       		return false;
       	}
    }
    
    public class PredefinedPieceCreator : IPieceCreator
    {
       	private readonly Queue<int> _queue;
       	public PredefinedPieceCreator(Queue<int> queue) => _queue = queue;
       	public bool TryCreate(bool lastPhase, int minTypeOnBoard, int amountSpawned, out IPiece piece)
       	{
       		if (_queue.Count > 0)
       		{
       			piece = new Piece(_queue.Dequeue());
       			return true;
       		}
       		piece = null;
       		return false;
       	}
    }
    
    public class FastFinishPieceCreator : IPieceCreator
    {
       	private readonly LevelData _leveData;
       	public FastFinishPieceCreator(LevelData leveData) => _leveData = leveData;
       	public bool TryCreate(bool lastPhase, int minTypeOnBoard, int amountSpawned, out IPiece piece)
       	{
       		if (lastPhase)
       		{
       			var t = Random.Range(minTypeOnBoard, _leveData.EndGValue(amountSpawned) - 1);
       			piece = new Piece(t);
       			return true;
       		}
       		piece = null;
       		return false;
       	}
    }
    
    public class LastPhasePieceCreator : IPieceCreator
    {
	    private readonly LevelData _leveData;
	    private readonly float[] _normalizePercent;
	    public LastPhasePieceCreator(LevelData leveData)
	    {
		    _leveData = leveData;
		    
		    var list = new List<float>();
		    float sum = 0;
		    float dist = _leveData.EndValue - _leveData.StartedValue;
		    for (var i = _leveData.StartedValue; i <= _leveData.EndValue; i++)
		    {
			    var step = (i - _leveData.StartedValue) / dist;
			    sum += leveData.FinishGenerateCurve.Evaluate(step);;
			    list.Add(sum);
		    }
		    _normalizePercent = new float[list.Count];
		    for (var i = 0; i < list.Count; i++)
		    {
			    _normalizePercent[i] = list[i] / sum;
		    }
	    }
	    public bool TryCreate(bool lastPhase, int minTypeOnBoard, int amountSpawned, out IPiece piece)
	    {
		    if (lastPhase)
		    {
			    piece = new Piece(GetRnd());
			    return true;
		    }
		    piece = null;
		    return false;
	    }
	    private int GetRnd()
	    {
		    var r = Random.Range(0, 1.0f);
		    var i = 0;
		    while (r > _normalizePercent[i])
		    {
			    i++;
		    }
		    return _leveData.StartedValue + i;
	    }
    }
}
