namespace Model {

	public struct PiecePosition {
		public IPiece piece;
		public BoardPos pos;
	}

}