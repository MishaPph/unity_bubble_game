﻿using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using UI;
using UnityEngine;
using View;

public interface IScoreSpawner
{
    void SpawnScore(VisualPiece piece, IPiece value);
    void AddScore(int value);
}

public class GameController : MonoBehaviour, IScoreSpawner
{
    public enum GameStatus:byte
    {
        Run,
        InFire,
        End
    }
    private Board _board;
    [SerializeField] private BoardRenderer _boardRenderer;
    
    [SerializeField] private TouchReceiver _touchReceiver;
    [SerializeField] private RayCastRender _rayCastRender;

    [SerializeField] private Progress _progress;
    [SerializeField] private UserPiecesView _piecesView;

    [SerializeField] private HudManager _hudManager;
    [SerializeField] private BlinkLabel _blinkPerfect;
    [SerializeField] private BlinkLabel _blinkFail;
    
    public GameStatus Status = GameStatus.Run;

    public int Score => _progress.Value;
    
    public event Action<bool, int> GameFinished;
    public void SetBoard(Board board, IPieceSpawner pieceSpawner)
    {
        _board = board;
        Status = GameStatus.Run;

        _progress.Set(1, 0);
        _piecesView.CreatePieces(board, pieceSpawner);
        _blinkPerfect.Hide();
        _blinkFail.Hide();
        _hudManager.Clear();
        _hudManager.Cache(board, _boardRenderer.Db);
        StopAllCoroutines();
    }

    private void Update()
    {
        if(Dialog.Instance.Pause.Visible)
            return;
        _rayCastRender.Visible = _touchReceiver.TouchExist && Status == GameStatus.Run;

        if (Status != GameStatus.Run) return;
        
        if (_touchReceiver.TouchExist || _touchReceiver.TouchReleased)
        {
            var fire = _rayCastRender.DrawTouch(Input.mousePosition, _piecesView.CurrentColor, out var list,
                out var point);
            if (fire)
            {
                if (_touchReceiver.TouchReleased)
                {
                    _touchReceiver.TouchExist = false;
                    StartCoroutine(PlayAnimation(list, point));
                }
            }
        }
    }
    
    private IEnumerator PlayAnimation(IReadOnlyList<Vector2> points, Vector2Int point)
    {
        var timefly = _piecesView.MoveByPath(points);

        Status = GameStatus.InFire;
        
        yield return new WaitForSeconds(timefly);
        
        var result = _board.PutPiece(point.x, point.y, _piecesView.Current);

        _piecesView.HideCurrent();
        _boardRenderer.Execute(result, this, FinishAnimation);
    }

    public void SpawnScore(VisualPiece piece, IPiece value)
    {
        _hudManager.Create(piece.AnchorPosition, value.Value, _boardRenderer.Db.GetColorForPiece(value));
        _progress.Add(piece.Piece.Value);
    }
    public void AddScore(int value)
    {
        _progress.Add(value);
    }

    private void FinishAnimation()
    {
        if (_board.FirstRowEmpty())
        {
            _blinkPerfect.Show();
            EndGame(true);
        } else if (_board.LastRowNotEmpty())
        {
            _blinkFail.Show();
            EndGame(false);
        }
        else
        {
            Status = GameStatus.Run;
            _piecesView.Next();
        }
    }

    private void EndGame(bool win)
    {
        Status = GameStatus.End;
        _piecesView.SetNextPieceVisible(false);
        _hudManager.ClearCache();
        //victory   
        GameFinished?.Invoke(win, Score);
    }
}
