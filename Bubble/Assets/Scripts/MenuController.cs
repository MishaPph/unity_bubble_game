using System;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public event Action RunGame;
    public event Action<bool> BloomChanged;
    
    public void Play()
    {
        RunGame?.Invoke();
    }

    public void BloomToggle(bool bloom)
    {
        BloomChanged?.Invoke(bloom);
    }
}
