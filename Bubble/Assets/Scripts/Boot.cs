﻿using Audio;
using Levels;
using Model;
using UI;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;
using View;

public class Boot : MonoBehaviour, IGameController {

    public enum ScreenId
    {
        Menu,
        Level,
        Game
    }
    [SerializeField] private BoardRenderer boardRenderer;
    [SerializeField] private Button restartButton;
    [SerializeField] private GameController _gameController;
    [SerializeField] private GameObject _gamePhysBorder;
    [SerializeField] private LevelController _levelController;
    [SerializeField] private GameObject _gameBackGround;
    [SerializeField] private MenuController _menuController;
    [SerializeField] private Volume volume;
    
    private int CurrentLevel { get; set; } = 1;
    
    private ScreenId Screen { get; set; }

    public ScreenId StartScreen;
    
    private void Start () {
        
        Input.multiTouchEnabled = false;
        Application.targetFrameRate = 60;

        _levelController.OnChooseLevel += StartLevel;
        _gameController.GameFinished += OnGameFinished;
        _menuController.RunGame += RunGame;
        _menuController.BloomChanged += BloomChanged;
        
        CurrentLevel = UserData.LastStartedLevel;
        
        if (StartScreen == ScreenId.Game)
        {
            StartLevel(CurrentLevel);
        } else
            ShowScreen(StartScreen);
    }

    private void BloomChanged(bool bloom)
    {
        if (volume.profile.TryGet(out Bloom  bloomLayer))
        {
            bloomLayer.active = bloom;
        }
    }

    private void RunGame()
    {
        StartLevel(CurrentLevel);
    }

    private void OnGameFinished(bool win, int score)
    {
        UserData.SetLevelScore(CurrentLevel, Mathf.Max(UserData.GetLevelScore(CurrentLevel), score));
        
        if (!win)
        {
            Invoke(nameof(ShowFailDialog), 3.0f);
            return;
        }
        
        if (CurrentLevel == UserData.CompletedLevels)
        {
            UserData.CompletedLevels++;
        }
        Invoke(nameof(ShowWinDialog), 3.0f);
    }

    private void ShowFailDialog()
    {
        SoundManager.Instance.Play(SoundId.Fail);
        Dialog.Instance.Finish.Show(false, CurrentLevel, _gameController.Score, this);
    }
    
    private void ShowWinDialog()
    {
        SoundManager.Instance.Play(SoundId.Success);
        Dialog.Instance.Finish.Show(true, CurrentLevel, _gameController.Score, this);
    }

    private void Update()
    {
        if (!Input.GetKeyDown(KeyCode.Escape)) return;
        
        if (Screen == ScreenId.Game)
        {
            if(_gameController.Status != GameController.GameStatus.End)
                Dialog.Instance.ChangePauseVisibility(this);
        }
        else if (Screen == ScreenId.Level)
        {
            ShowScreen(ScreenId.Menu);
        }
        else if (Screen == ScreenId.Menu)
        {
            //TODO: ask for quit
            Application.Quit();
        }
    }
    
    public void Menu()
    {
        ShowScreen(ScreenId.Menu);
    }

    public void Levels()
    {
        ShowScreen(ScreenId.Level);
    }

    public void NextLevel()
    {
        CurrentLevel++;
        StartLevel(CurrentLevel);
    }
    
    private void ShowScreen(ScreenId screen)
    {
        Screen = screen;
        
        _gameBackGround.SetActive(Screen == ScreenId.Game);
        _gameController.gameObject.SetActive(Screen == ScreenId.Game);
        _gamePhysBorder.SetActive(Screen == ScreenId.Game);
        
        _levelController.gameObject.SetActive(Screen == ScreenId.Level);
        _menuController.gameObject.SetActive(Screen == ScreenId.Menu);
    }

    private void StartLevel(int level)
    {
        ShowScreen(ScreenId.Game);
        UserData.LastStartedLevel = level;
        CurrentLevel = level;
        var (board, piece) = CreateBoardAndGoal(LevelManager.Instance.GetLevelData(CurrentLevel));
        boardRenderer.Initialize(board);
        _gameController.SetBoard(board, piece);
        restartButton.onClick.AddListener(Restart);
    }

    private static (Board, IPieceSpawner) CreateBoardAndGoal(LevelData levelData)
    {
        var (boardData, pieceSpawner) = levelData.Generate();
        var board = Board.Create(boardData, pieceSpawner);
        board.CountAddTopRow = levelData.CountAddRow;
        board.RemoveUnconnected(new ResolveResult());
        return (board, pieceSpawner);
    }

    public void Restart() {
        var (board, piece) = CreateBoardAndGoal(LevelManager.Instance.GetLevelData(CurrentLevel));
        boardRenderer.ResetBoard(board);
        _gameController.SetBoard(board, piece);
    }
}