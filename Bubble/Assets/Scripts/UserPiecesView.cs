﻿using System;
using System.Collections.Generic;
using Audio;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins;
using DG.Tweening.Plugins.Core.PathCore;
using DG.Tweening.Plugins.Options;
using Model;
using UnityEngine;
using View;

public class UserPiecesView:MonoBehaviour
{
    private static readonly Vector3 lowSize = new Vector3(0.8f, 0.8f, 0.8f);
    public const float Speed_Fly = 0.00020f;

    [SerializeField] private PieceTypeDatabase _pieceDatabase;
    [SerializeField] private UserVisualPiece _current;
    [SerializeField] private VisualPiece _next;
    
    [SerializeField] private VisualPiece _maxPiece;

    public Color CurrentColor => _current.Color;
    public IPiece Current => _currentPiece;

    public PieceTypeDatabase Db => _pieceDatabase;
    
    private IPiece _currentPiece;
    private IPiece _nextPiece;
    
    private IPieceSpawner _pieceSpawner;
    
    private Vector3 _startPosition;
    private Board _board;

    private void Awake()
    {
        _startPosition = _current.AnchorPosition;
    }

    public void CreatePieces(Board board, IPieceSpawner pieceSpawner)
    {
        Clear();
        
        _maxPiece.Set(pieceSpawner.MaxValue.ToString(), Db.GetColorForPieceType(pieceSpawner.Max));
        
        _board = board;
        _pieceSpawner = pieceSpawner;
        _nextPiece = _pieceSpawner.CreateUserPiece(_board.CountAddTopRow == 0, _board.GetMinPieces());
        _current.PlayIdle();
        _current.gameObject.SetActive(false);
        SetNextPieceVisible(true);
        Invoke(nameof(Next) ,0.1f);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
    public void Next()
    {
        SoundManager.Instance.Play(SoundId.Spawn);
        
        _current.gameObject.SetActive(true);
        _currentPiece = _nextPiece;

        _nextPiece = _pieceSpawner.CreateUserPiece(_board.CountAddTopRow == 0, _board.GetMinPieces());
        
        _current.Set(_currentPiece, _pieceDatabase.GetColorForPiece(_currentPiece));
        _next.Set(_nextPiece, _pieceDatabase.GetColorForPiece(_nextPiece));

        _next.transform.localScale = Vector3.zero;
        _next.transform.DOScale(lowSize, 0.6f).SetEase(Ease.OutElastic);
        
        _current.transform.localScale = lowSize;
        _current.MoveToReady(_next.transform.position);
    }

    private TweenerCore<Vector3, Path, PathOptions> _tween;
    
    public float MoveByPath(IReadOnlyList<Vector2> points)
    {
        _current.StopIdle();
        var wayPoint = ArrayUtility.CastListToPath(points, out var length);
        var timeFly = length*Speed_Fly;
        _tween  = DOTween.To(PathPlugin.Get(),  (() => _current.AnchorPosition), (x => _current.AnchorPosition = x), 
            new Path(PathType.Linear, wayPoint, 1, null), timeFly).SetTarget(_current).SetEase(Ease.Linear);
        _tween.plugOptions.mode = PathMode.TopDown2D;
        _tween.plugOptions.useLocalPosition = true;
        return timeFly;
    }
    public void HideCurrent()
    {
        _current.Clear();
        _current.AnchorPosition = _startPosition;
        _current.gameObject.SetActive(false);
    }

    public void SetNextPieceVisible(bool visible)
    {
        _next.gameObject.SetActive(visible);
    }

    private void Clear()
    {
        CancelInvoke();
        _tween.Kill(false);
        HideCurrent();
    }
}