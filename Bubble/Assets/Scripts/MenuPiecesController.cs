using System;
using System.Collections.Generic;
using Libs;
using UnityEngine;
using View;
using Random = UnityEngine.Random;

public class MenuPiecesController : MonoBehaviour
{
    [SerializeField] private Transform _spawnPoint;
    
    [MinMaxSlider(10, 200)] public Vector2Int DelayRange = new Vector2Int(1, 50);

    [SerializeField] private float _spawnRadius = 50;
    
    [SerializeField] private MenuVisualPiece _piecePrefab;
    
    [SerializeField] private float _delay;

    private Queue<MenuVisualPiece> _pool = new Queue<MenuVisualPiece>();
        
    private List<Tuple<float, MenuVisualPiece>> _toHide = new List<Tuple<float, MenuVisualPiece>>();
    
    private float _time;
   
   private void Update()
   {
       _time += Time.deltaTime;
       if (_time > _delay)
       {
           _time -= _delay;
           _delay = Random.Range(DelayRange.x, DelayRange.y) * 0.01f;
           Spawn();
       }

       var i = 0;
       for (i = 0; i < _toHide.Count; i++)
       {
           if(_toHide[i].Item1 > Time.time)
               break;
           _toHide[i].Item2.gameObject.SetActive(false);
           _pool.Enqueue(_toHide[i].Item2);
       }
       _toHide.RemoveRange(0, i);
   }

   private void Spawn()
   {
       var pos = Random.insideUnitCircle*_spawnRadius + (Vector2)_spawnPoint.position;
       var piece = CreatePiece(pos);
       piece.SetColor(Color.HSVToRGB(Random.Range(0, 1.0f), 0.8f, 0.8f));
       piece.transform.SetParent(transform);
       _toHide.Add(Tuple.Create(Time.time + 15, piece));
   }

   private MenuVisualPiece CreatePiece(Vector3 position)
   {
       if (_pool.Count > 0)
       {
           var p = _pool.Dequeue();
           p.transform.position = position;
           p.gameObject.SetActive(true);
           return p;
       }
       var piece = Instantiate(_piecePrefab,  position, Quaternion.identity);
       return piece;
   }
   
   private void OnDisable()
   {
       foreach (var t in _toHide)
       {
           t.Item2.gameObject.SetActive(false);
           _pool.Enqueue(t.Item2);
       }
       _toHide.Clear();
   }
}
