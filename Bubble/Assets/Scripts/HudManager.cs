﻿using System.Collections;
using System.Collections.Generic;
using Model;
using UnityEngine;

public class HudManager : MonoBehaviour
{
    [SerializeField] private HudText _hudTextPrefab;
    private Dictionary<int, List<HudText>> _hudCache = new Dictionary<int, List<HudText>>();

    private WaitForSeconds _timeToHide = new WaitForSeconds(2.0f);
    
    public void Create(Vector2 anchorPosition, int value, Color color)
    {
        var rd = Create(value, color);
        rd.Set(anchorPosition);
        StartCoroutine(Hide(rd, value));
    }

    private IEnumerator Hide(HudText hudText, int value)
    {
        yield return _timeToHide;
        hudText.gameObject.SetActive(false);
        AddToCache(hudText, value);
    }
    
    private void AddToCache(HudText hudText, int value)
    {
        if (_hudCache.TryGetValue(value, out var list))
        { 
            list.Add(hudText);
        }
        else
        {
            _hudCache.Add(value,  new List<HudText>{hudText}); 
        }
    }
    
    private HudText Create(int value, Color color)
    {
        if (_hudCache.TryGetValue(value, out var list))
        {
            if (list.Count > 0)
            {
                var item = list[0];
                item.gameObject.SetActive(true);
                list.RemoveAt(0);
                return item;
            }
        }
        var rd = Instantiate(_hudTextPrefab, transform);
        rd.Set(value.ToString(), color);
        return rd;
    }

    public void Cache(Board board, PieceTypeDatabase database)
    {
        var count = new Dictionary<int, (IPiece , int)>();
        
        foreach (var b in  board.IteratePieces())
        {
            var v = b.piece.Value;
            if (count.TryGetValue(v, out var p))
            {
                count[v] = (b.piece, p.Item2 + 1);
            }
            else
            {
                count.Add(v, (b.piece, 1));
            }
        }

        foreach (var un in count)
        {
            Cache(un.Key, database.GetColorForPiece(un.Value.Item1), un.Value.Item2/2 + 1);
        }
    }

    public void Cache(int value, Color color, int count)
    {
        List<HudText> list;
        if (!_hudCache.ContainsKey(value))
        {
            list = new List<HudText>();
            _hudCache.Add(value, list);
        } else
        {
            _hudCache.TryGetValue(value, out list);
        }
        for (var i = 0; i < count; i++)
        {
            var rd = Instantiate(_hudTextPrefab, transform);
            rd.Set(value.ToString(), color);
            rd.gameObject.SetActive(false);
            list.Add(rd);
        }
    }
    
    public void ClearCache()
    {
        foreach (var dict in _hudCache)
        {
            foreach (var item in dict.Value)
            {
                Destroy(item.gameObject);
            }
        }
        _hudCache.Clear();
    }

    public void Clear()
    {
        StopAllCoroutines();
        ClearCache();
        foreach (Transform tr in transform)
        {
            Destroy(tr.gameObject);
        }
    }
}
