﻿using Audio;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BlinkLabel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI[] _words;

    [SerializeField] private HorizontalLayoutGroup _layoutGroup;

    [SerializeField] private SoundId Sound = SoundId.Label;
    
    private Color _color;
    private void Awake()
    {
        _color = _words[0].color;
    }

    [ContextMenu("Show")]
    public virtual void Show()
    {
        gameObject.SetActive(true);
        _layoutGroup.CalculateLayoutInputHorizontal();
        _layoutGroup.SetLayoutHorizontal();
        
        foreach (var word in _words)
        {
            word.color = Color.clear;
        }

        Invoke(nameof(ShowText), 0.3f);
    }

    private void ShowText()
    {
        float maxTime = 0;
        foreach (var word in _words)
        {
            var delay = Random.Range(0.0f, 0.5f);
            var duration = Random.Range(0.4f, 1.2f);
            Animate(word, duration, delay, _color, null);
            maxTime = Mathf.Max(maxTime, delay + duration);
        }
        Invoke(nameof(PlaySound), maxTime - 2.8f);
    }

    private void PlaySound()
    {
        SoundManager.Instance.Play(Sound);
    }
    public static void Animate(TextMeshProUGUI word, float duration, float delay, Color color, TweenCallback onComplete)
    {
        word.outlineWidth = 0.032f;
        const float endValue = 0.2f;
        const float endValue2 = 0.182f;
        var tween = DOTween.Sequence();
        var show = DOTween.To(() => word.outlineWidth, (f) => word.outlineWidth = f, endValue, duration).SetEase(Ease.InOutBounce).SetDelay(delay);

        var show2 = DOTween.To(() => word.outlineWidth, (f) => word.outlineWidth = f, endValue2, duration).SetEase(Ease.OutElastic)
            .SetDelay(delay/2);
        
        var showColor = DOTween.To(() => word.color, (f) => word.color = f, color, duration/2)
            .SetEase(Ease.Linear).SetDelay(delay);
        
        tween.Append(show).Join(showColor);
        tween.Append(show2).OnComplete(onComplete);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
