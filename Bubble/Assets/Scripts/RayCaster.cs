﻿using System.Collections.Generic;
using UnityEngine;

public class RayCaster: MonoBehaviour
{
    [SerializeField] private Camera _worldCamera;

    private RaycastHit2D[] Hits = new RaycastHit2D[15];

    public Vector2 ToWorld(Vector2 point)
    {
        return _worldCamera.ScreenToWorldPoint(point);
    }
    
    public Vector2 ToScreen(Vector2 point)
    {
        return _worldCamera.WorldToScreenPoint(point);
    }

    private const float HitOffset = 25;
    public bool GetHits(Vector2 from, Vector2 direction, out List<Vector2> list, out Vector2 hitPoint)
    {
        list = new List<Vector2>{from};
        var k = 0;
        while (k++ < 7)
        {
            var hitCount = Physics2D.RaycastNonAlloc(from, direction, Hits);
            var minDist = GetMinHitPosition(list, hitCount, out var piece, out var hitId);
            if (piece)
            {
                list.Add(minDist);
                hitPoint = minDist + direction * 30 + new Vector2(30, 0);
                return true;
            }
            from = minDist;
            direction.x *= -1;
            from += direction*HitOffset;
            list.Add(from);
        }
        hitPoint = Vector2.zero;
        return false;
    }

    private Vector2 GetMinHitPosition(List<Vector2> list, int hitCount, out bool piece, out int hitId)
    {
        var minDist = Vector2.zero;
        var min = float.MaxValue;
        var coll = Hits[0].collider;
        var l = list[list.Count - 1];
        hitId = 0;
        for (var i = 0; i < hitCount; i++)
        {
            var hit = Hits[i];
            var f = hit.point;
            var d = Vector2.Distance(f, l);
            if (!(d < min)) continue;
            min = d;
            coll = hit.collider;
            minDist = f;
            hitId = i;
        }
        piece = false;
        if (coll != null && (coll.tag.Equals("Piece") || coll.tag.Equals("Top")))
        {
            piece = true;
        }
        return minDist;
    }
    
    /*private void OnGUI()
    {
        GUI.color = Color.black;
        for (var i = 0; i < Hits.Length; i++)
        {
            GUI.Label(new Rect(20, 20 +i*40, 180, 40), Hits[i].point.ToString());
        }
    }*/

    private void OnDrawGizmos()
    {
        for (var i = 0; i < Hits.Length; i++)
        {
            if(Hits[i].collider == null)
                continue;
            Gizmos.DrawWireSphere(Hits[i].point, 13f);
           // GUI.Label(new Rect(20, 20 +i*40, 180, 40), Hits[i].point.ToString());
        }
    }
}