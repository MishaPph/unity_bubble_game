using UnityEngine;
using UnityEngine.UI;

namespace View {
    public class MenuVisualPiece : MonoBehaviour {
        
        [SerializeField] private Image _image;
        public void SetColor(Color color)
        {
            _image.color = color;
        }
    }
}