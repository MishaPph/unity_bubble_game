﻿using Model;
using UnityEngine;

namespace View
{
    public static class MathHelper
    {
        public static readonly Vector2 PieceSize = new Vector2(142, 124);
        private const float HeightOffset = 1680 + 250;
		
        public static Vector2 LogicPosToVisualPos(int x, int y) { 
            return new Vector2(x*PieceSize.x + (RowHelper.IsRow(y)?0:76) + PieceSize.x, -y*PieceSize.y + HeightOffset);
        }

        public static Vector2 VisualPosToScreenPos(Vector2 visualPos) {
            return visualPos;
        }
		
        public static Vector2Int ScreenPosToLogicPos(Vector2 screenPos)
        {
            var y = Mathf.RoundToInt((screenPos.y - HeightOffset) / PieceSize.y);
            var row = RowHelper.IsRow(y)?0:76;
            var x   = Mathf.RoundToInt((screenPos.x - PieceSize.x - row)/PieceSize.x);
            return new Vector2Int(x, -y);
        }
		
        public static Vector2 ScreenPosToVisualPos(Vector2 screenPos) {
            return screenPos;
        }

        public static Vector2 LogicPosToScreen(int x, int y)
        {
            return VisualPosToScreenPos(LogicPosToVisualPos(x, y));
        }
    }
}