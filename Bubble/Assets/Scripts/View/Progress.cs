﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace View
{
    public class Progress : MonoBehaviour
    {
        private const int LevelScoreStep = 64;

        [SerializeField] private TextMeshProUGUI _textFrom;
        [SerializeField] private TextMeshProUGUI _textTo;
        [SerializeField] private TextMeshProUGUI _value;

        [SerializeField] private Transform _progress;

        private bool _inAnimation;
        private int _to;
        private float MaxValue => LevelScoreStep * Mathf.Pow(_to, 2);
        private float MinValue => _to > 0 ? LevelScoreStep * Mathf.Pow(_to - 1, 2):0;
        
        private int _lastValue;

        public int Value { get; private set; }

        public void Set(int to, int value)
        {
            _inAnimation = false;
            _textFrom.text = (to - 1).ToString();
            _textTo.text = to.ToString();
            _lastValue = Value = value;
            _to = to;
            SetValue(0);
        }

        public void SetProgress(float s)
        {
            _progress.localScale = new Vector3(s, 1, 1);
        }

        public void Add(int score)
        {
            Value += score;
            if(!_inAnimation)
                StartCoroutine(AnimProgress());
        }
        
        private void SetValue(int value)
        {
            SetValueLabel(value);
            
            while (value >= MaxValue)
            {
                NextLevel();
            }
            SetProgress((value - MinValue) / (MaxValue - MinValue));
        }

        private void SetValueLabel(int value)
        {
            if (value < 2000)
            {
                _value.text = value.ToString();
            }
            else if(value < 2000000)
            {
                var v = value / 1000.0f;
                _value.text = $"{v:0.##} k";
            }
            else
            {
                var v = value / 1000000.0f;
                _value.text = $"{v:0.##} M";
            }
        }
        
        private IEnumerator AnimProgress()
        {
            _inAnimation = true;
            while (_lastValue < Value)
            {
                var delta = (int)((Value - _lastValue) * 0.1f) + 2;
                _lastValue = Mathf.Min(Value, _lastValue + delta);
                SetValue(_lastValue);
                yield return new WaitForEndOfFrame();
            }
            _inAnimation = false;
        }
        
        private void NextLevel()
        {
            _to++;
            _textFrom.text = (_to - 1).ToString();
            _textTo.text = _to.ToString();
        }
    }
}
