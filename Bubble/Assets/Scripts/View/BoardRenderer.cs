﻿using System;
using System.Collections;
using System.Collections.Generic;
using Audio;
using Model;
using UnityEngine;

namespace View
{
	public class BoardRenderer : MonoBehaviour {
		
		[SerializeField] private PieceTypeDatabase pieceTypeDatabase;
		[SerializeField] private VisualPiece visualPiecePrefab;

		public PieceTypeDatabase Db => pieceTypeDatabase;
		private Board _board;

		private VisualPiece[,] _visualPieces;
		private readonly Queue<VisualPiece> _poolList = new Queue<VisualPiece>();

		public void Initialize(Board board) {
			if (_board != null)
			{
				Clear();
			}
			_board = board;
			_visualPieces = new VisualPiece[board.Width, board.Height];
			CreateVisualPiecesFromBoardState();
		}

		private void Clear()
		{
			StopAllCoroutines();
			foreach (var vp in _poolList)
			{
				Destroy(vp.gameObject);
			}
			_poolList.Clear();
			DestroyVisualPieces();
			for (var i = 0; i < _board.Width; i++) {
				for (var j = 0; j < _board.Height; j++) {
					_visualPieces[i, j] = null;
				}
			}
		}
		public void ResetBoard(Board board)
		{
			Clear();
			_board = board;
			CreateVisualPiecesFromBoardState();
		}

		private void CreateVisualPiecesFromBoardState() {
			foreach (var pieceInfo in _board.IteratePieces()) {
				if(pieceInfo.piece.type == 0)
					continue;
				CreateVisualPiece(pieceInfo.piece, pieceInfo.pos.x, pieceInfo.pos.y);
			}
		}

		private VisualPiece CreateVisualPiece(IPiece piece, int x, int y)
		{
			var visualPiece = CreateVisualPiece(piece);
			_visualPieces[x, y] = visualPiece;
			visualPiece.AnchorPosition = MathHelper.LogicPosToVisualPos(x, y);
			return visualPiece;
		}

		private void MoveToDestroy(int x, int y, Vector2 to)
		{
			if (_visualPieces[x, y] == null) return;
			
			_poolList.Enqueue(_visualPieces[x, y]);
			_visualPieces[x, y].MoveToOther(to, 0.15f);
			_visualPieces[x, y] = null;
		}
		
		private void DestroyVisualPiece(int x, int y)
		{
			if (_visualPieces[x, y] == null) return;
			
			_poolList.Enqueue(_visualPieces[x, y]);
			_visualPieces[x, y].PlayDestroy();
			_visualPieces[x, y] = null;
		}
		
		private VisualPiece ChangeVisualPiece(IPiece piece, int x, int y)
		{
			if (_visualPieces[x, y] != null) {
				_visualPieces[x, y].ChangeTo(piece, pieceTypeDatabase.GetColorForPiece(piece));
			}
			return _visualPieces[x, y];
		}
		

		private VisualPiece CreateVisualPiece(IPiece piece) {
			VisualPiece pieceObject;
			if (_poolList.Count > 0) {
				pieceObject = _poolList.Dequeue();
				pieceObject.gameObject.SetActive(true);
			}
			else {
				pieceObject = Instantiate(visualPiecePrefab, transform, false);
			}
			var color = pieceTypeDatabase.GetColorForPiece(piece);
			pieceObject.Set(piece, color);
			return pieceObject;
		}

		private void DestroyVisualPieces() {
			for (var i = 0; i < _board.Width; i++) {
				for (var j = 0; j < _board.Height; j++)
				{
					if (_visualPieces[i, j] == null) continue;
					Destroy(_visualPieces[i, j].gameObject);
				}
			}
		}

		private void MovePiece(BoardPos from, BoardPos to)
		{
			_visualPieces[from.x, from.y].MoveTo(MathHelper.LogicPosToVisualPos(to.x, to.y));
			_visualPieces[to.x, to.y] = _visualPieces[from.x, from.y];
			_visualPieces[from.x, from.y] = null;
		}
		
		public void Execute(ResolveResult result, IScoreSpawner scoreSpawner, Action finished)
		{
			StartCoroutine(ExecuteAnim(result, scoreSpawner, finished));
		}

		private IEnumerator ExecuteAnim(ResolveResult result, IScoreSpawner scoreSpawner, Action finished)
		{
			var (piece, boardPos) = result.AddedPiece;
			var visualPiece = CreateVisualPiece(piece);
			visualPiece.AnchorPosition = MathHelper.LogicPosToVisualPos(boardPos.x, boardPos.y);
			_visualPieces[boardPos.x, boardPos.y] = visualPiece;
			
			PlayPunch(boardPos.x, boardPos.y);
			SoundManager.Instance.Play(SoundId.Hit);
			
			yield return new WaitForSeconds(0.03f);
			
			if(result.movement.Count > 0)
				RowHelper.RowIndex++;

			foreach (var stepChanges in result.changes)
			{
				var bp = Vector2.zero;
				bool haveChanged = false;
				foreach (var post in stepChanges)
				{
					if (post.Value.State != ChangeState.Changed) continue;
					var r = ChangeVisualPiece(post.Key, post.Value.FromPos.x, post.Value.FromPos.y);
					bp = r.AnchorPosition;
					haveChanged = true;
				}

				if (haveChanged)
				{
					foreach (var post in stepChanges)
					{
						if (post.Value.State == ChangeState.Removed)
						{
							scoreSpawner.SpawnScore(_visualPieces[post.Value.FromPos.x, post.Value.FromPos.y], post.Key);
							MoveToDestroy(post.Value.FromPos.x, post.Value.FromPos.y, bp);
						}
					}
				}
				else
				{
					foreach (var post in stepChanges)
					{
						if (post.Value.State == ChangeState.Removed)
						{
							scoreSpawner.SpawnScore(_visualPieces[post.Value.FromPos.x, post.Value.FromPos.y], post.Key);
							DestroyVisualPiece(post.Value.FromPos.x, post.Value.FromPos.y);
						}
					}
				}
				if(stepChanges.Count > 0)
					yield return new WaitForSeconds(0.3f);
			}

			var dropeed = false;
			foreach (var post in result.dropped)
			{
				if (post.State != ChangeState.Dropped)
					continue;

				var vpiece = _visualPieces[post.FromPos.x, post.FromPos.y];
				vpiece.Drop(460, () =>
				{
					scoreSpawner.SpawnScore(vpiece, vpiece.Piece);
				});
				_visualPieces[post.FromPos.x, post.FromPos.y] = null;
				dropeed = true;
			}

			if (dropeed)
			{
				yield return new WaitForSeconds(0.8f);
			}
			
			foreach (var post in result.movement)
			{
				MovePiece(post.FromPos, post.ToPos);
			}
			
			foreach (var post in result.created)
			{
				if (post.Value.State != ChangeState.Created) continue;
				var visual = CreateVisualPiece(post.Key, post.Value.ToPos.x, post.Value.ToPos.y);
				var anch = visual.AnchorPosition;
				anch.y += MathHelper.PieceSize.y;
				visual.AnchorPosition = anch;
				visual.MoveTo(MathHelper.LogicPosToVisualPos(post.Value.ToPos.x, post.Value.ToPos.y), true);
			}
			if(result.movement.Count > 0)
				yield return new WaitForSeconds(0.6f);
			finished();
		}

		private void PlayPunch(int x, int y)
		{
			var pieces= _board.ConvertToPiecePosition(_board.GetNeighbors(x, y, RowHelper.IsRow(y)));
			var from = _visualPieces[x, y].AnchorPosition;
			foreach (var p in pieces)
			{
				var piece = _visualPieces[p.pos.x, p.pos.y];
				if (piece == null)
				{
					continue;
				}
				var direction = piece.AnchorPosition - from;
				piece.PlayKnock(direction.normalized, 20);
			}
		}
		public Vector2 GetCloserFreePiecePosition(Vector2 screenHitPoint, Vector2 prevHit, out Vector2Int pos)
		{
			var pair = MathHelper.ScreenPosToLogicPos(screenHitPoint);
			return CloserFreePiecePosition(pair.x, pair.y, prevHit, out pos);
		}

		private Vector2 CloserFreePiecePosition(int x, int y, Vector2 prevHit, out Vector2Int pos)
		{
			x = Mathf.Clamp(x, 0, _board.Width - 1);
			y = Mathf.Clamp(y, 0, _board.Height - 1);
			
			pos = new Vector2Int(x, y);
			if (_board.GetAt(x, y) == null)
			{
				return MathHelper.LogicPosToScreen(x, y);
			}
			var s = MathHelper.ScreenPosToVisualPos(prevHit);
			var minDistance = float.MaxValue;
			foreach (var l in _board.GetEmptyNeighbors(x, y, RowHelper.IsRow(y)))
			{
				var d = Vector2.Distance(MathHelper.LogicPosToVisualPos(l.x, l.y), s);
				if (!(minDistance > d)) continue;
				minDistance = d;
				pos = l;
			}

			return MathHelper.LogicPosToScreen(pos.x, pos.y);
		}

		private static Vector2 ToWorld(Vector2 point)
		{
			return Camera.main.ScreenToWorldPoint(point);
		}
		
		private void OnDrawGizmos()
		{
			/*if (_lastPiece != null)
			{
				Gizmos.color = Color.red;
				
				var (x, y) = GetLogicPosition(_lastPiece);
				
				Gizmos.DrawWireSphere(_lastPiece.transform.position, 45.0f);
				Gizmos.DrawSphere(ToWorld(LogicPosToVisualPos(x,y)), 25.0f);

				Gizmos.color = Color.green;
				
				foreach (var l in _board.GetEmptyNeighbors(x, y, RowHelper.IsRow(y)))
				{ 
					Gizmos.DrawSphere(ToWorld(LogicPosToVisualPos(l.x, l.y)), 20);
				}
			}*/
			Gizmos.color = Color.cyan;
			if(_board != null)
				foreach (var piece in _board.GetUnConnected())
				{
					if (_board.TryGetPiecePos(piece, out var x, out var y))
					{
						Gizmos.DrawSphere(ToWorld(MathHelper.LogicPosToVisualPos(x, y)), 20);
					}else
					{
						Debug.LogError(piece);
					}
				}
		}
	}
}