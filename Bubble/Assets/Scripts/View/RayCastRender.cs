﻿using System.Collections.Generic;
using UnityEngine;

namespace View
{
    public class RayCastRender : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private VisualPiece _piece;
        [SerializeField] private RayCaster _rayCaster;
        [SerializeField] private RectTransform _center;
        [SerializeField] private BoardRenderer _boardRenderer;

        [SerializeField, Range(0.0f, 0.2f)] private float _bottomViewPoint;
        
        private RectTransform _pieceTransform;

        private void Awake()
        {
            _pieceTransform = _piece.GetComponent<RectTransform>();
        }

        public bool Visible
        {
            get => _lineRenderer.enabled;
            set
            {
                _lineRenderer.enabled = value;
                _piece.gameObject.SetActive(value);
            }
        }

        public bool DrawTouch(Vector2 screenPoint, Color pieceColor, out List<Vector2> list, out Vector2Int pos)
        {
            screenPoint.y = Mathf.Max((float)screenPoint.y/Screen.height, _bottomViewPoint)*Screen.height;
            
            var fromPoint = (Vector2)_center.position;
            var direction = (_rayCaster.ToWorld(screenPoint) - fromPoint).normalized;
            _lineRenderer.startColor = _lineRenderer.endColor = pieceColor;
            var endcolor = pieceColor;
            endcolor.a = 0;
            _lineRenderer.endColor = endcolor;
            
            var piece = _rayCaster.GetHits(fromPoint, direction, out list, out var screenHitPoint);
            DrawLine(list);
            if (piece)
            {
                _piece.SetColor(pieceColor);
                var touchPiecePosition =
                    _boardRenderer.GetCloserFreePiecePosition(screenHitPoint, list[list.Count - 1], out pos);
                _pieceTransform.anchoredPosition = touchPiecePosition;
                list[list.Count - 1] = touchPiecePosition;
                return true;
            }
            pos = new Vector2Int();
            return false;
        }

        private void DrawLine(List<Vector2> points)
        {
            if (points.Count < 2) return;
            points[0] += MathHelper.PieceSize * (points[1] - points[0]).normalized*0.5f;
            
            _lineRenderer.positionCount = points.Count;
            for (var i = 0; i < points.Count; i++)
            {
                _lineRenderer.SetPosition(i, points[i]);
            }
        }
    }
}
