using System;
using System.Collections;
using Audio;
using DG.Tweening;
using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace View {
    public class VisualPiece : MonoBehaviour {
        private const float FallingSpeed = 200f;
        
        [SerializeField] protected Animation _animation;
        [SerializeField] private Image _image;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private ParticleSystem _destroyFxPrefab;
        [SerializeField] private ParticleSystem _moveFxPrefab;
        public Color Color => _image.color;
        public IPiece Piece { get; private set; }

        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get
            {
                if(_rectTransform == null)
                    _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }

        public Vector2 AnchorPosition
        {
            get=> RectTransform.anchoredPosition;
            set => RectTransform.anchoredPosition = value;
        }

        public void Set(string value, Color color)
        {
            SetColor(color);
            SetTextValue(value);
        }
        public  void Set(IPiece piece, Color color)
        {
            Piece = piece;
            SetColor(color);
            SetTextValue(piece.GetStringValue());
            
            StopAllCoroutines();
            if (piece.IsSpecial)
            {
                StartCoroutine(AnimColor());
            }
        }

        public virtual void SetColor(Color color)
        {
            _image.color = color;
        }
        
        public void ChangeTo(IPiece piece, Color color) {
            _image.color = color;
            Piece = piece;
            SetTextValue(piece.GetStringValue());
            RectTransform.DOPunchScale(Vector3.one * 1.01f, 0.2f).SetDelay(0.1f);
            var tweenUp =  DOTween.To(()=>Mult, SetColorMult, 1.5f, 0.2f);
            var tweenDown =  DOTween.To(()=>Mult, SetColorMult, 1.3f, 0.1f);
            var mySequence = DOTween.Sequence();
            mySequence.Append(tweenUp).Append(tweenDown);
        }

        private IEnumerator AnimColor()
        {
            _image.color = Color.HSVToRGB(Random.Range(0, 1.0f), 1.0f, 0.8f);
            while (true)
            {
                Color.RGBToHSV(_image.color, out var h, out var s, out var v);
                s =  Mathf.Clamp01(0.8f + Mathf.Cos(Time.time)*0.05f);
                h += 0.007f;
                v = Mathf.Clamp01(0.8f + Mathf.Cos(Time.time)*0.1f);
                SetColor(Color.HSVToRGB(h, s, v));
                yield return new WaitForEndOfFrame();
            }
        }
        
        private void SetTextValue(string value)
        {
            _text.text = value;
        }
        
        private static readonly int ColorMult = Shader.PropertyToID("_Mult");

        public float Mult { get; private set; } = 1.3f;

        public void SetColorMult(float value)
        {
            Mult = value;
            _image.material.SetFloat(ColorMult, 1.3f);
        }

        public void MoveTo(Vector2 localPosition, bool spawn = false)
        {
            var duration = Vector2.Distance(AnchorPosition, localPosition) / FallingSpeed;
            RectTransform.DOAnchorPos(localPosition, duration).SetEase(Ease.OutBounce);
            if (spawn)
            {
                RectTransform.localScale = Vector3.zero;
                RectTransform.DOScale(Vector3.one, duration).SetEase(Ease.OutBounce);
            }
        }
        
        public void PlayDestroy()
        {
            SoundManager.Instance.Play(SoundId.Destroy);
            PlayDestroyFx();
            RectTransform.DOKill();
            gameObject.SetActive(false);
        }

        private void PlayDestroyFx()
        {
            var ps = Instantiate(_destroyFxPrefab);
            var main = ps.main;
            main.startColor = new ParticleSystem.MinMaxGradient(Color);
            ps.Play();
            ps.transform.position = gameObject.transform.position;
            Destroy(ps.gameObject, 4.0f);
        }
        
        public void Drop(float boarderY, Action dropped)
        {
            var dist = Mathf.Abs(AnchorPosition.y - boarderY);
            var finPos = new Vector2(AnchorPosition.x + Random.Range(-dist/4, dist/4), boarderY);
            RectTransform.DOAnchorPos(finPos, dist * 0.001f).OnComplete(() =>
            {
                dropped?.Invoke();
                SoundManager.Instance.Play(SoundId.FallHit);
                PlayDestroyFx();
                Destroy(gameObject);
            }).SetEase(Ease.InCubic);
        }

        public void PlayKnock(Vector2 direction, float value)
        {
            RectTransform.DOPunchAnchorPos(direction*value, 0.1f, 1).SetEase(Ease.OutCubic);;
        }

        public void MoveToOther(Vector2 to, float duration)
        {
            var ps = Instantiate(_moveFxPrefab);
            var main = ps.main;
            main.startColor = new ParticleSystem.MinMaxGradient(Color);
            ps.Play();
            ps.transform.position = gameObject.transform.position;
            Destroy(ps.gameObject, 5.0f);
            
            RectTransform.DOAnchorPos(to, duration).OnComplete(PlayDestroy).SetEase(Ease.InOutSine);
        }
    }
}