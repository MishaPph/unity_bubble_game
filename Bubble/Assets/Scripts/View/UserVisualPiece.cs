﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class UserVisualPiece : VisualPiece
    {
        [SerializeField] private Image _glow;
        [SerializeField] private TrailRenderer _trail;
        [SerializeField] private ParticleSystem _trailFx;
        public override void SetColor(Color color)
        {
            base.SetColor(color);
            _glow.color = color;
            _trail.startColor = color;
            _trail.endColor = color;
            var main = _trailFx.main;
            main.startColor = new ParticleSystem.MinMaxGradient(Color);
        }

        public void Clear()
        {
            var em = _trailFx.emission;
            em.enabled = false;
            _trail.Clear();
        }

        public void MoveToReady(Vector3 from)
        {
            _trail.enabled = false;
            
            transform.DOScale(Vector3.one, 0.4f).SetEase(Ease.OutBounce);
            var pos = transform.position;
            transform.position = from;
            transform.DOMove(pos, 0.45f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                _trail.enabled = true;
                var em = _trailFx.emission;
                em.enabled = true;
                _trailFx.Play();
                PlayIdle();
            });
        }
        
        public void PlayIdle()
        {
            _animation.Play();
        }

        public void StopIdle()
        {
            _animation.Stop();
        }
    }
}