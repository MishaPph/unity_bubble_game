﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchReceiver : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerClickHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        TouchPressed = true;
        TouchReleased = false;
        TouchExist = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!TouchPressed)
            return;
        TouchExist = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TouchExist = false;
    }

    public bool TouchExist {get ; set; }
    public bool TouchReleased {get ; private set; }
    public bool TouchPressed {get ; private set; }
    
    public void OnPointerUp(PointerEventData eventData)
    {
        //TouchReleased = true;
        TouchPressed = false;
    }

    private void FixedUpdate()
    {
        if (!TouchReleased) return;
        
        TouchExist = false;
        TouchReleased = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        TouchReleased = true;
    }
}
