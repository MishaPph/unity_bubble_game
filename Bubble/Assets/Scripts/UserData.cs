using UnityEngine;

public static class UserData
{
   public static int CompletedLevels
   {
      get => PlayerPrefs.GetInt("CompletedLevels", 1);
      set => PlayerPrefs.SetInt("CompletedLevels", value);
   }

   public static int LastStartedLevel
   {
      get => PlayerPrefs.GetInt("LastStartedLevel", 1);
      set => PlayerPrefs.SetInt("LastStartedLevel", value);
   }

   public static int GetLevelScore(int level)
   {
      return PlayerPrefs.GetInt("LevelScore"+level, 0);
   }
   
   public static void SetLevelScore(int level, int score)
   {
      PlayerPrefs.SetInt("LevelScore" + level, score);
   }
}
