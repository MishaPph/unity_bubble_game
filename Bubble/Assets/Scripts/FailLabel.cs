using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FailLabel : BlinkLabel
{
    [SerializeField] private Image _back;

    [ContextMenu("ShowFail")]
    public override void Show()
    {
        base.Show();
        _back.gameObject.SetActive(true);
        var c = _back.color;
        var c1 = c;
        c1.a = 1;
        c.a = 0;
        _back.color = c;
        _back.DOColor(c1, 1.0f);
    }


    public override void Hide()
    {
        base.Hide();
        
        _back.gameObject.SetActive(false);
    }
}
