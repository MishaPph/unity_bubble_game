﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private Animation _anim;
        
    private static readonly int UnderlayColor = Shader.PropertyToID("_UnderlayColor");

    private static Dictionary<Color, Material> _cacheMaterials = new Dictionary<Color, Material>();
    
    public void Set(Vector2 anchorPosition)
    {
        _anim.Play();
        _rectTransform.anchoredPosition = anchorPosition;
    }

    public void Set(Vector2 anchorPosition, string text, Color color)
    {
        _rectTransform.anchoredPosition = anchorPosition;
        Set(text, color);
    }
    
    public void Set(string text, Color color)
    {
        _text.text = text;
        _text.outlineColor = color;
        if (_cacheMaterials.TryGetValue(color, out var material))
        {
            _text.fontSharedMaterial = material;
            _text.material = material;
        }
        else
        {
            _text.fontSharedMaterial.SetColor(UnderlayColor, color);
            var mtl = _text.fontSharedMaterial;
            mtl.name = color.ToString();
            _text.material = mtl;
            _cacheMaterials.Add(color,  mtl);
        }
    }
}