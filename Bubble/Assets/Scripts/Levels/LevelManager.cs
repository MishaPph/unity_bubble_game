using Libs;
using UnityEngine;

namespace Levels
{
    public class LevelManager : Singleton<LevelManager>
    {
        [SerializeField] private LevelData[] _levels;

        public int Count => _levels.Length;
        public LevelData GetLevelData(int level)
        {
            return _levels[level - 1];
        }
    }
}
