using System;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Levels
{
   public class LevelController : MonoBehaviour
   {
      private const int  CountLevelPerPage = 20;
         
      [SerializeField] private Transform _content;
      [SerializeField] private LevelItem _itemPrefab;
      [SerializeField] private Image _colorImage;
      [SerializeField] private Button _next;
      [SerializeField] private Button _prev;
      
      private readonly List<LevelItem> _items = new List<LevelItem>();
      public event Action<int> OnChooseLevel;

      private int _page = 0;
      private int _countPage = 4;

      public void Start()
      {
         _countPage = LevelManager.Instance.Count / CountLevelPerPage;
            
         _next.onClick.AddListener(Next);
         _prev.onClick.AddListener(Prev);
         
         for (var i = 0; i < CountLevelPerPage; i++)
         {
            _items.Add(Instantiate(_itemPrefab, _content));
         }

         Refresh();
      }

      private void Update()
      {
         Color.RGBToHSV( _colorImage.color, out var h, out var s, out var v);
         h += Time.deltaTime * 0.02f;
         v += Time.deltaTime * 0.0093f;
         _colorImage.color = Color.HSVToRGB(h, s, v);
      }

      private void OnEnable()
      {
         if(_items.Count > 0)
            Refresh();
      }

      public void Refresh()
      {
         for (var i = 0; i < CountLevelPerPage; i++)
         {
            var l = i + 1 + _page * CountLevelPerPage;
            var completed = l < UserData.CompletedLevels;
            var showScore = completed || l == LevelManager.Instance.Count;
            _items[i].Set(l, l > UserData.CompletedLevels, completed, showScore, ChooseLevel);
         }
         RefreshButton();
      }

      private void ChooseLevel(int level)
      {
         OnChooseLevel?.Invoke(level);
      }

      private void Next()
      {
         _page++;
         Refresh();
      }

      private void Prev()
      {
         _page--;
         Refresh();
      }

      private void RefreshButton()
      {
         _next.interactable = _page != _countPage;
         _prev.interactable = _page != 0;
      }
   }
}
