﻿using System;
using Model;
using UnityEngine;

namespace Levels
{
    [Serializable]
    public class SpecialPieceData
    {
        [Range(0, 100)] public int Percent;
        public SpecialId Id;
        [Range(0, 30)] public int After;
    }
}