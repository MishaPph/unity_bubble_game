﻿using System;
using System.Collections.Generic;
using Model;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Levels
{
    public static class BoardDataHelper
    {
        public static void Generate(ref int[,] boardDefinition, IPieceSpawner spawner, AnimationCurve rowCurve)
        {
            var sizeX = boardDefinition.GetLength(0);
            for (var y = 0; y < boardDefinition.GetLength(1); y++) {
                for (var x = 0; x < Random.Range(1, sizeX); x++)
                {
                    boardDefinition[x, y] = spawner.CreateBasicPiece().type;
                    var r = Random.Range(0.0f, 1.0f);
                    if (rowCurve.Evaluate((float)x/sizeX) < r)
                    {
                        boardDefinition[x, y] = 0;
                    }
                }
            }
        }
        
        public static void FillText(ref int[,] boardDefinition, string text)
        {
            var data = text.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.None);
            for (var x = 0; x < boardDefinition.GetLength(0); x++)
            {
                if (x >= data.Length)
                    break;

                var str = data[x].ToCharArray();

                for (var y = 0; y < boardDefinition.GetLength(1) && y < str.Length; y++)
                {
                    var type = int.Parse(str[y].ToString());
                    boardDefinition[x, y] = type;
                }
            }
        }

        public static Queue<int> ToPiecesQueue(string str)
        {
            var userPieces = new Queue<int>(str.Length);
            foreach (var c in str)
            {
                if (c > '9')
                {
                    var k = c - 'A' + 1;
                    userPieces.Enqueue(Const.SpecialOffset + k);
                }
                else
                {
                    userPieces.Enqueue(c - '0');
                }
            }
            return userPieces;
        }
        
        public static Queue<int> ToPiecesQueueWithTrim(string text)
        {
            return ToPiecesQueue(text.Replace("\n",""));
        }
    }
}