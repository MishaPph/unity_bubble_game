using System;
using Libs;
using Model;
using UnityEngine;

namespace Levels
{
    [CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order = 1)]
    public class LevelData : ScriptableObject
    {
        [Range(2, 5)]  public int PowBase = 2;
        
        [MinMaxSlider(1,11)] public Vector2Int Range;
        public int StartedValue => Range.x;
        public int EndValue =>  Range.y;
        
        [Range(2, 11)] public int MaxValue;

        [Tooltip("When there are only a few pieces left, generate only good pieces")]
        public bool FastFinish = false;
        public AnimationCurve FinishGenerateCurve  = AnimationCurve.Linear(0,1,1,1);
        
        [Space]
        [Header("Predefined data")]
        [Multiline(8)] public string BoardData = "";
        
        public string UserPieceQueue = "";
        
        [Space]
        [Header("Top Spawn data")]
        public int CountAddRow = 1;
        [TextArea(3, 8)] public string AdditionalData = "";

        public SpecialPieceData[] Specials;

        public AnimationCurve GenerateRowCurve = AnimationCurve.EaseInOut(0,1,1,0);
       
        public AnimationCurve GenerateValue = AnimationCurve.Linear(0,1,1,1);
        public int CountSpawned = 0;

        public int SpawnRange => EndValue - StartedValue;

        public int EndGValue(int move)
        {
            if (CountSpawned == 0 || move > CountSpawned)
                return EndValue;
            var sim = SpawnRange * GenerateValue.Evaluate((float)move / CountSpawned);
            return StartedValue + (int)sim;
        }
        
        public int Max => Mathf.Max(EndValue + 1, MaxValue);
        
        private int[,] _boardDefinition = {
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0},
        };

        public (int[,], IPieceSpawner) Generate()
        {
            var pieceSpawner = new PieceSpawner(this);
            Array.Clear(_boardDefinition, 0, _boardDefinition.Length);
            if (BoardData.Length > 8)
            {
                BoardDataHelper.FillText(ref _boardDefinition, BoardData);
            }
            else
            {
                BoardDataHelper.Generate(ref _boardDefinition, pieceSpawner, GenerateRowCurve);
            }
            return (_boardDefinition, pieceSpawner);
        }
    }
}
